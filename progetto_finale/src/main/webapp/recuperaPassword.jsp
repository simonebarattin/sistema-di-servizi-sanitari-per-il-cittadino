<%-- 
    Document   : recuperaPassword
    Created on : 3 Nov 2019, 15:53:16
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %> 

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Recupero Password</title>
<link rel="icon" href="images/stethoscope.png"/>
<link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
    .w3-muli {
        font-family: 'Muli', sans-serif;
        }
</style>
</head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<body>
<header>
    <div >
    <h1><img style="width:100%" src="images/header landing page.png" alt="header"></h1>
    </div>
</header>
    
<c:if test="${not empty emailSent}">
    <c:choose>
        <c:when test="${emailSent eq true}">
            <div class="alert alert-success alert-dismissible" role="alert">
                <strong>Controlla la tua mail</strong>
            </div>
        </c:when>
        <c:otherwise>
            <section style="width:100%; height:350px; margin-left:35%">
                <div style="margin-left:50px">
                    <h2 class="w3-muli">LOGIN</h2>
                    <form class="w3-muli" action= "cambiaPassword" method="POST">
                        <p>Inserisci la mail per cambiare la password <%=request.getParameter("emailSent")%></p><br>
                        Email <br><input type = "text" name = "mail_cambio"><br>
                        <input type="hidden" name="type" value="<%= request.getParameter("type") %>">
                        <br>
                        <div style="margin-left:25px"> <button class="w3-muli w3-button w3-teal w3-round-xxlarge" type="submit" style="border-radius:12px">Invia</button></div><br>
                    </form>
                </div>
            </section>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> Failed to send the e-mail: ${emailError}.
            </div>
        </c:otherwise>
    </c:choose>
</c:if>
<c:if test="${empty emailSent}">
    <section style="width:100%; height:350px; margin-left:35%">
        <div style="margin-left:50px">
            <h2 class="w3-muli">LOGIN</h2>
            <form class="w3-muli" action= "cambiaPassword" method="POST">
                <p>Inserisci la mail per cambiare la password <%=request.getParameter("emailSent")%></p><br>
                Email <br><input type = "text" name = "mail_cambio"><br>
                <input type="hidden" name="type" value="<%= request.getParameter("type") %>">
                <br>
                <div style="margin-left:25px"> <button class="w3-muli w3-button w3-teal w3-round-xxlarge" type="submit" style="border-radius:12px">Invia</button></div><br>
            </form>
        </div>
    </section>
</c:if>

<footer class="w3-container w3-teal">
    <img style="float:left; margin-left: 150px; height:200px;" src="images/lotusfooter.png" alt="logo">
    <div style="float:right; margin-right:20%; height:225px;">
    <h5>SIP PSSR</h5>
    <small><p>AREA Science Park, 973 Don Jackson Lane<br>
        96826, Honolulu, HI <br>
        sipsanitaria@sip.com<br>
        P.IVA/ Cod.Fisc./ Reg.Imprese Honolulu 00707090326 - Capitale Sociale € 6.258.327,60 i.v.<br>
        Societ&agrave; soggetta all’attivit&agrave; di direzione e coordinamento da parte di SIP Group
        <br><br>
        <a class="" href="privacy.html">Privacy</a>
    </p></small>
    </div>
</footer>

</body>
</html>
