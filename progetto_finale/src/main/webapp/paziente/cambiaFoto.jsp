<%-- 
    Document   : cambiaFoto
    Created on : 28 Dec 2019, 18:37:55
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <title>Paziente - Modifica Foto</title>
    <link rel="icon" href="../../images/stethoscope.png"/>
    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .w3-muli {
            font-family: 'Muli', sans-serif;
            }
    </style>
    </head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <body>

        <header>
            <div >
                <h1><img style="width:100%" src="../../images/header paziente.png" alt="header"></h1>
            </div>
        </header>
        
<div class="w3-top">
  <div class="w3-bar w3-turquoise w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
  </div>
</div> 

        <!-- Sidebar -->
        <nav class="w3-sidebar w3-flat-turquoise w3-bar-block w3-collapse w3-large w3-animate-left" id="mySidebar">
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
<i class="fa fa-remove"></i></a>

            <h4 class="w3-bar-item"><b>Menu</b></h4>
            <form method="post" action="paziente.handler">
            <input type="hidden" name="command" value="ESAMI">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Esami">
        </form>
        <form method="post" action="paziente.handler">
            <input type="hidden" name="command" value="VISITE">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Visite Mediche">
        </form>
        <form method="post" action="paziente.handler">
            <input type="hidden" name="command" value="RICETTE">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Ricette">
        </form>
        <form method="post" action="paziente.handler">
            <input type="hidden" name="command" value="TICKET">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Ticket">
        </form>
        <form method="post" action="paziente.handler">
            <input type="hidden" name="command" value="CAMBIO">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Cambia medico">
        </form>
        <form method="post" action="${contextPath}/progetto_finale/PLogoutServlet">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Logout">
        </form>
        </nav>
            
            <!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div> 
<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">
    

        <!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
        <div class="w3-twothird w3-container">
            <h5>Cambia Foto Profilo</h5>
            <form method="post" action="paziente.handler" enctype="multipart/form-data">
                Select image to upload:
                <input class="w3-muli w3-teal w3-round-xxlarge" type="file" name="fileToUpload" id="fileToUpload"><br>
                <input  class="w3-muli w3-button w3-teal w3-round-xxlarge" type="submit" value="Upload Image" name="submit">
            </form>
        </div>  
        
        <script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
//var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>
    </body>
</html>
