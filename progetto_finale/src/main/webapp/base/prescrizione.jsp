<%-- 
    Document   : schedaPaziete
    Created on : 1 ott 2019, 11:47:43
    Author     : pietr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="it">
<head>
<link rel="stylesheet" type="text/css" href="..\..\css\style.css">
<title>Scheda Paziente</title>
<link rel="icon" href="..\..\images\stethoscope.png"/>
<link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .w3-muli {
        font-family: 'Muli', sans-serif;
        }
</style>
</head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<body>

<header>
    <div >
        <h1><img style="width:100%" src="..\..\images\schedapaziente.png" alt="header"></h1>
    </div>
</header>

<!-- Sidebar -->
<nav class="w3-sidebar w3-flat-turquoise w3-bar-block w3-collapse w3-large w3-animate-left" id="mySidebar">
        <h4 class="w3-bar-item"><b>Menu</b></h4>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="PARCO">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Parco Pazienti">
        </form>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="ESAME">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Prescrizione Esame Specialistico">
        </form>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="SSP">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Prescrizioe Esame SSP ">
        </form>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="FARMACI">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Prescrizione Farmaci">
        </form>     
        <form method="post" action="${contextPath}/progetto_finale/MBLogoutServlet">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Logout">
        </form>
    </nav>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<div style="margin-left:20%" class="w3-twothird w3-container">
    <h2>Prescrizioni</h2>

    <c:forEach var="presc" items="${presc}">
        <ul><li><h4>${presc.nome} - ${presc.erog}</h4></li></ul>
    </c:forEach>
    
</div>
</body>