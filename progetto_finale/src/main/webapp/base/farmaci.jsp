<%-- 
    Document   : medicoBase
    Created on : 20 ago 2019, 09:30:37
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="it">
<head>
<link rel="stylesheet" type="text/css" href="..\..\css\style.css">
<title>PP</title>
<link rel="icon" href="..\..\images\stethoscope.png"/>
<link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .w3-muli {
        font-family: 'Muli', sans-serif;
    }
    input,select {
        border-radius: 4px;
        border: 2px solid #3cbfae;
        resize: none;
    }
</style>
</head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<body>

<header>
    <div >
        <h1><img style="width:100%" src="..\..\images\basefarmaci.png" alt="header"></h1>
    </div>
</header>

    <div class="w3-top">
  <div class="w3-bar w3-turquoise w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
  </div>
</div>
    
<!-- Sidebar -->
<nav class="w3-sidebar w3-flat-turquoise w3-bar-block w3-collapse w3-large w3-animate-left" id="mySidebar">
        <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
<i class="fa fa-remove"></i></a>
    <h4 class="w3-bar-item"><b>Menu</b></h4>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="PARCO">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Parco Pazienti">
        </form>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="ESAME">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Prescrizione Esame Specialistico">
        </form>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="SSP">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Prescrizioe Esame SSP ">
        </form>
        <form method="post" action="base.handler">
            <input type="hidden" name="command" value="FARMACI">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Prescrizione Farmaci">
        </form>     
        <form method="post" action="${contextPath}/progetto_finale/MBLogoutServlet">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Logout">
        </form>
    </nav>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div> 
<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px"></div> 

<!-- Ricerca -->
<div class="w3-twothird w3-container">
    <h2>Ricerca Farmaci</h2>
    
    <form action= "base.handler" method="post">
        <input type="hidden" name="command" value="SUBMITfarm">
        <input type="hidden" name="med_type" value="BASE">
    <c:set var="count" value="0" scope="page" />
        <select name="form1">
        <c:forEach var="item" items="${farmaci}">
            <option name="${count + 1}"  value="${item.id_farm}"> ${item.nome} </option> 
    </c:forEach> 
     
        </select> <br> <br>
        
    <c:set var="i" value="0" scope="page" />
        <select name="form2">
            
        <c:forEach var="paziente" items="${patients}">
            
            <option  value="${paziente.SSN} ${paziente.email} "> ${paziente.nome} ${paziente.cognome} </option> 
            
        </c:forEach>
        </select>
    </select> <br> <br>
    <input type="date" name="vdate" > <br> <br>
            <input class="close w3-muli w3-button w3-teal w3-round-xxlarge" type="submit" value="SUBMIT"> 
    </form>
 
</div>

  
  <script>
  function myFunction() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[1];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }
  </script>
 
  <script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
//var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>

</body>
</html>