<%-- 
    Document   : updateReport
    Created on : 15 Oct 2019, 16:23:06
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Report</title>
        <link rel="stylesheet" type="text/css" href="../../css/style.css">
        <link rel="icon" href="../../images/stethoscope.png"/>
        <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
        .w3-muli {
                font-family: 'Muli', sans-serif;
            }
        textarea {
                border-radius: 4px;
                border: 2px solid #3cbfae;
                resize: none;
            }
        
        </style>
    </head>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <body>
        
        <header>
            <div >
                <h1><img style="width:100%" src="../../images/headerssp.png" alt="header"></h1>
            </div>
        </header>
        
        <!-- Sidebar -->
        <nav class="w3-sidebar w3-flat-turquoise w3-bar-block w3-collapse w3-large w3-animate-left" id="mySidebar">
            <h4 class="w3-bar-item"><b>Menu</b></h4>
            <form method="post" action="ssp.handler">
                <input type="hidden" name="command" value="REPORT">
                <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Report">
            </form>
            <form method="post" action="ssp.handler">
                <input type="hidden" name="command" value="EROGA">
                <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Erogazione Ticket">
            </form>
            <form method="post" action="${contextPath}/progetto_finale/SSPLogoutServlet">
                <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Logout">
            </form>

        </nav>
        <!-- Overlay effect when opening sidebar on small screens -->
        <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <div style="margin-left:20%" class="w3-twothird w3-container">
        <form action="ssp.handler" method="post">
            <input type="hidden" name="mail_paziente" value="${esame_ssp.paz.email}">
            <input type="hidden" name="med_type" value="SSP">
            <input type="hidden" name="id_vssp" value="${esame_ssp.id_vssp}">
            <input type="hidden" name="command" value="UPDATE_REPORT"> 
            <p>ID visita: ${esame_ssp.id_vssp}</p>
            <p>Paziente: ${esame_ssp.paz.cognome} ${esame_ssp.paz.nome}</p>     
            <h6>Risultato:</h6> 
            <textarea rows="10" cols="75" name="risultato"></textarea><br>
            <input class="w3-muli w3-button w3-teal w3-round-xxlarge" type="submit" value="Aggiorna">
        </form>
        </div>
    </body>
</html>