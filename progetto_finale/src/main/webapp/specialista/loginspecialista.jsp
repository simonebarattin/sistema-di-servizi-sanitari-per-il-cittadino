<%-- 
    Document   : loginspecialista
    Created on : 7 Oct 2019, 15:44:57
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css">
<title>Area Medico Specialista</title>
<link rel="icon" href="../images/stethoscope.png"/>
<link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
    .w3-muli {
        font-family: 'Muli', sans-serif;
        }
</style>
</head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<body>
<header>
    <div >
    <h1><img style="width:100%" src="../images/header landing page.png" alt="header"></h1>
    </div>
</header>

    <%
        Cookie[] cookies = request.getCookies();
        Cookie cookie = null;
        String mail=null,pass=null;
        if( cookies != null ) {
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ms_mail"))
                    mail = cookie.getValue();
                if(cookie.getName().equals("ms_password"))
                    pass = cookie.getValue();
            }
        }
        request.setAttribute("mail", mail);
        request.setAttribute("pass", pass);
    %>

<section style="width:100%; height:350px; margin-left:15%">
    <div >
        <h2 class="w3-muli">LOGIN</h2>
        <form class="w3-muli" action= "../MSLoginServlet" method="POST">
            <c:choose>
                <c:when test="${empty mail && empty pass}">
                    Email <br><input type = "text" name = "email"><br>
                    Password <br> <input type = "password" name = "pass"><br><br>            
                </c:when>
                <c:otherwise>
                    Email <br><input type = "text" name = "email" value="${mail}"><br>
                    Password <br> <input type = "password" name = "pass" value="${pass}"><br><br>
                </c:otherwise>
            </c:choose>
            <input class="w3-muli" type="checkbox" name="type" value="ricordami" > Ricordami<br><br>
            <div style="margin-left:25px"> <button class="w3-muli w3-button w3-teal w3-round-xxlarge" type="submit" style="border-radius:12px">Log-in</button></div><br>
            <c:url var="tempLink" value="../recuperapassword.html">
                <c:param name="type" value="spec"></c:param>
            </c:url>
            <a href="${tempLink}">Hai dimenticato la password?</a>
        </form>
        <c:if test="${not empty cambioPass}">
            <c:choose>
                <c:when test="${cambioPass eq true}">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        La password &egrave; stata<strong> modificata correttamente!</strong>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Errore!</strong> La password non &egrave; stata modificata.
                    </div>
                </c:otherwise>
            </c:choose>
        </c:if>
    </div>
</section>

<footer class="w3-container w3-teal">
    <img style="float:left; margin-left: 150px; height:200px;" src="../images/lotusfooter.png" alt="logo">
    <div style="float:right; margin-right:20%; height:225px;">
    <h5>SIP PSSR</h5>
    <small><p>AREA Science Park, 973 Don Jackson Lane<br>
        96826, Honolulu, HI <br>
        sipsanitaria@sip.com<br>
        P.IVA/ Cod.Fisc./ Reg.Imprese Honolulu 00707090326 - Capitale Sociale € 6.258.327,60 i.v.<br>
        Societ&agrave; soggetta all’attivit&agrave; di direzione e coordinamento da parte di SIP Group
        <br><br>
        <a class="" href="../privacy.html">Privacy</a>
    </p></small>
    </div>
</footer>

</body>
</html>
