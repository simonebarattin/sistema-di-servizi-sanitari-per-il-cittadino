<%-- 
    Document   : update
    Created on : 22 set 2019, 16:10:06
    Author     : simon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../../css/style.css">
        <link rel="icon" href="../../images/stethoscope.png"/>
        <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
        .w3-muli {
            font-family: 'Muli', sans-serif;
            }
        textarea {
                border-radius: 4px;
                border: 2px solid #3cbfae;
                resize: none;
            }
        form{
            text-align: center;                
            }
        header{
            width: 100%;
            background-color: #3cbfae;
            margin-top: 0%;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
    <body>
        
        <header>
        <div>
            <h1 style="text-align:center; color: whitesmoke"> ${datiVisita.paz.nome} ${datiVisita.paz.cognome} <br> Visita prescritta dal Dott. ${datiVisita.mb.nome} ${datiVisita.mb.cognome}</h1>
        </div>
        </header>
        <br>
        <div style="text-align: center"> 
            <h4> Esame: ${datiVisita.esame} </h4>
            <h5> Amnesi:</h5>
        <form method="post" action="specialista.handler">
            <textarea rows="15" cols="50" name="anamnesi"></textarea>
            <input type="hidden" name="command" value="UPDATE">
            <input type="hidden" name="id" value="${datiVisita.id_vs}">
            <br>
            <input class="w3-muli w3-button w3-teal w3-round-xxlarge" type="submit" style="border-radius:12px">
            <input class="w3-muli w3-button w3-teal w3-round-xxlarge" type="reset" style="border-radius:12px">
        </form>
        </div>
    </body>
</html>
