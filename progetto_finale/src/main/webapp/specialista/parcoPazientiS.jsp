<%-- 
    Document   : parcoPazientiS
    Created on : 24 ago 2019, 21:15:34
    Author     : simon
--%>

<%@page import="com.sip_project.progetto_finale.persistence.entities.Paziente"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="it">
    <head>
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <title>PPS</title>
    <link rel="icon" href="../../images/stethoscope.png"/>
    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .w3-muli {
            font-family: 'Muli', sans-serif;
            }
    </style>
    </head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <body>

    <header>
        <div >
            <h1><img style="width:100%" src="../../images/parcopazienti.png" alt="header"></h1>
        </div>
    </header>
        
<div class="w3-top">
  <div class="w3-bar w3-turquoise w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
  </div>
</div>

    <!-- Sidebar -->
    <nav class="w3-sidebar w3-flat-turquoise w3-bar-block w3-collapse w3-large w3-animate-left" id="mySidebar">
        <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
        <i class="fa fa-remove"></i></a>
        <h4 class="w3-bar-item"><b>Menu</b></h4>
        <form method="post" action="specialista.handler">
            <input type="hidden" name="command" value="PARCO">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Parco Pazienti">
        </form>
        <form method="post" action="specialista.handler">
            <input type="hidden" name="command" value="VISITA">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Visite">
        </form>
        <form method="post" action="specialista.handler">
            <input type="hidden" name="command" value="TICKET">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Erogazione Ticket">
        </form>     
        <form method="post" action="${contextPath}/progetto_finale/MSLogoutServlet">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Logout">
        </form>
    </nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div> 
<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

    <!-- Ricerca -->
    <div class="w3-twothird w3-container">
        <h2>Pazienti</h2>
        <p>Ricerca un paziente</p>

        <input class="w3-input w3-border w3-padding" type="text" placeholder="Cerca per nome...." id="myInput" onkeyup="myFunction()">

        <table class="w3-table-all w3-margin-top" id="myTable">
            <thead>
            <tr class="w3-teal">
              <th style="width:60%;">Nome e Cognome</th>
              <th style="width:40%;">Scheda Paziente</th>
            </tr>
            <c:forEach var="patient" items="${patients}">
                
                <c:url var="tempLink" value="specialista.handler">
                    <c:param name="command" value="LOAD" />
                    <c:param name="pazienteSSN" value="${patient.SSN}" />
                </c:url>
                
                <tr>
                    <td>${patient.nome} ${patient.cognome}</td>
                    <td><a href="${tempLink}">Vedi Scheda</a></td>
                </tr>
            </c:forEach>    
          </thead>
        </table>
        
        <c:if test="${not empty emailSent}">
            <c:choose>
                <c:when test="${emailSent eq true}">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        E-mail <strong>correctly</strong> sent!
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Error!</strong> Failed to send the e-mail: ${emailError}.
                    </div>
                </c:otherwise>
            </c:choose>
        </c:if>

      </div>

      <script>
      function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
      </script>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
//var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>
      
    </body>
</html>
