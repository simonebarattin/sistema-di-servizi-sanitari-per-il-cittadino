<%-- 
    Document   : schedaPaziente
    Created on : 26 ago 2019, 16:53:31
    Author     : simon
--%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <title>Scheda Paziente</title>
    <link rel="icon" href="../../images/stethoscope.png"/>
    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-flat.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .w3-muli {
            font-family: 'Muli', sans-serif;
            }
    </style>
    </head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <body>

    <header>
        <div >
            <h1><img style="width:100%" src="../../images/parcopazienti.png" alt="header"></h1>
        </div>
    </header>

        <div class="w3-top">
  <div class="w3-bar w3-turquoise w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
  </div>
</div>
        
    <!-- Sidebar -->
    <nav class="w3-sidebar w3-flat-turquoise w3-bar-block w3-collapse w3-large w3-animate-left" id="mySidebar">
        <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
<i class="fa fa-remove"></i></a>
        <h4 class="w3-bar-item"><b>Menu</b></h4>
        <form method="post" action="specialista.handler">
            <input type="hidden" name="command" value="PARCO">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Parco Pazienti">
        </form>
        <form method="post" action="specialista.handler">
            <input type="hidden" name="command" value="VISITA">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Visite">
        </form>
        <form method="post" action="specialista.handler">
            <input type="hidden" name="command" value="TICKET">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Erogazione Ticket">
        </form>    
        <form method="post" action="${contextPath}/progetto_finale/MSLogoutServlet">
            <input type="submit" class="w3-bar-item w3-button w3-hover-black" value="Logout">
        </form>
    </nav>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div> 
<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

        <div class="w3-row w3-padding-30">
            <div class="w3-twothird w3-container">
                <div class="w3-card w3-round w3-white">
                <!-- Profile -->
                    <div class="w3-container">
                        <h4 class="w3-center">Profilo Paziente</h4>
                        <p class="w3-center"><img src="../../images/avatars/${schedaPaziente.getFoto()}" class="w3-circle" style="height:159px;width:24%; cursor:zoom-in" onclick="document.getElementById('modal01').style.display='block'" alt="Avatar"></p>
                        <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
                            <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
                            <div style="width:40%" class="w3-modal-content w3-animate-zoom">
                              <img src="../../images/avatars/${schedaPaziente.getFoto()}" style="width:100%">
                            </div>
                        </div>
                        <hr>
                        <p><i class="fa far fa-user fa-fw w3-margin-right w3-text-theme"></i> Cognome: ${schedaPaziente.getCognome()}</p>
                        <p><i class="fa far fa-user fa-fw w3-margin-right w3-text-theme"></i> Nome: ${schedaPaziente.getNome()}</p>
                        <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i> Data di nascita: ${schedaPaziente.getData()}</p>
                        <p><i class="fa fas fa-id-card fa-fw w3-margin-right w3-text-theme"></i> Codice Fiscale: ${schedaPaziente.getSSN()}</p>
                        <p><i class="fa fas fa-venus-mars fa-fw w3-margin-right w3-text-theme"></i> Sesso: ${schedaPaziente.getSex()}</p>
                        <p><i class="fa fas fa-user-md fa-fw w3-margin-right w3-text-theme"></i> Medico di base: ${schedaPaziente.getDoc()}</p>
                        <p><i class="fa fa-at fa-fw w3-margin-right w3-text-theme"></i> Email: ${schedaPaziente.getEmail()}</p>
                        <p><i class="fa fa-bullseye fa-fw w3-margin-right w3-text-theme"></i> Provincia: ${schedaPaziente.getProvincia()}</p>
                    </div>
                </div>
            </div>
            <br>
        </div>
                    
        <div class="w3-twothird w3-container">
            <h2>Storico visite paziente</h2>
            <table class="w3-table-all w3-margin-top" id="myTable">
                <thead>
                    <tr class="w3-teal">
                      <th style="width:40%;">Esame</th>
                      <th style="width:20%;">Data</th>
                      <th style="width:40%;">Medico Base</th>
                    </tr>
                    <c:forEach var="visita" items="${visitePaz}">
                        <tr>
                            <td>${visita.esame}</td>
                            <td>${visita.getErog()}</td>
                            <td>Dott. ${visita.mb.cognome} ${visita.mb.nome}</td>
                        </tr>
                    </c:forEach>    
                </thead>
            </table>
        </div>
    </div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
//var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>
                    
    </body>
</html>
