package com.sip_project.progetto_finale.listeners;

import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.commons.persistence.dao.factories.jdbc.JDBCDAOFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebAppContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String dburl = sce.getServletContext().getInitParameter("dburl");
        try {
            JDBCDAOFactory.configure(dburl);
            DAOFactory daoFactory = JDBCDAOFactory.getInstance();
            sce.getServletContext().setAttribute("daoFactory", daoFactory);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(WebAppContextListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DAOFactory daoFactory = (DAOFactory) sce.getServletContext().getAttribute("daoFactory");
        if (daoFactory != null) {
            daoFactory.shutdown();
            System.out.println("\n\n\n\n---------Connection closed----------\n\n\n\n\n");
        }
        
        daoFactory = null;
    }
}
