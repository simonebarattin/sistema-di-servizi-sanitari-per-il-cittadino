/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.SSPDAO;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import com.sip_project.progetto_finale.persistence.entities.VisitaSSP;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class SSPServlet extends HttpServlet {
    
    private SSPDAO sspDao;
    private Cookie cookie = null;
    private Cookie[] cookies = null;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for storage system");
        }
        try {
            sspDao = daoFactory.getDAO(SSPDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        } 
    }
    
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String command = request.getParameter("command");
        switch(command){
            case "TICKET":
                loadReport(request,response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String command = request.getParameter("command");
        if(command == null)
            command = "";
        switch(command){
            case "REPORT":
                getReport(request,response);
                break;
            case "EROGA":
                prepErog(request,response);
                break;
            case "UPDATE_REPORT":
                updateReport(request,response);
                break;
        }
    }

    private void getReport(HttpServletRequest request, HttpServletResponse response) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        String provincia = null;
        cookies = request.getCookies();
        for(int i=0;i<cookies.length;i++){
            cookie = cookies[i];
            if(cookie.getName().equals("ssp_provincia")){
                provincia = cookie.getValue();
            }
        }
        try {
            List<PrescrizioneFarmaco> farmaci = sspDao.getPrescrizioni(provincia);
            request.getSession().setAttribute("farmaci", farmaci);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/ssp/export2Excel");
            rd.forward(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void prepErog(HttpServletRequest request, HttpServletResponse response) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        /*
        for(int i=0;i<cookies.length;i++){
            cookie = cookies[i];
            if(cookie.getName().equals("id_ssp"))
                id_ssp = cookie.getValue();
        */
        int id_ssp = 1;
        try {
            List<VisitaSSP> esami = sspDao.getEsamiCompleti(id_ssp);
            request.getSession().setAttribute("esami",esami);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/ssp/listaesami.html");
            rd.forward(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadReport(HttpServletRequest request, HttpServletResponse response) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            int id_vssp = Integer.parseInt(request.getParameter("id_vssp"));
            VisitaSSP vms = sspDao.getVisitByPrimaryKey(id_vssp);
            request.setAttribute("esame_ssp", vms);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/ssp/updatereport.html");
            rd.forward(request, response);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateReport(HttpServletRequest request, HttpServletResponse response) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            sspDao.updateReport(request.getParameter("risultato"), Integer.parseInt(request.getParameter("id_vssp")));
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/ssp/home.html"));
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
