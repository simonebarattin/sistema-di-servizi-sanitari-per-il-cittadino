/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import java.io.IOException;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class QRServlet extends HttpServlet{
    
    private PazienteDAO pDao;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for storage system");
        }
        try {
            pDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for patient storage system", ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String text = "ID medico: "+request.getParameter("id_mb")+",\nCodice fiscale paziente: "+request.getParameter("ssn")+",\nTimestamp prescrizione:"
                + request.getParameter("time_presc")+",\nCodice prescrizione: "+request.getParameter("id_presc")+",\nNome farmaco: "+ request.getParameter("farmaco");
        response.setContentType("image/png");
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(getQRCodeImage(text, 400, 400));
        outputStream.flush();
        outputStream.close();
    }
    
    private byte[] getQRCodeImage(String text, int width, int height){
        try{
            QRCodeWriter qRCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qRCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }catch(Exception e){
            return null;
        }
    }
}
