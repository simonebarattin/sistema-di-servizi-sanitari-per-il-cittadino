/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author simon
 */
public class Encrypt{
    
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public Encrypt(){}
    
    public String getHash(String pass, String salt) throws NoSuchAlgorithmException{
        String input = pass+salt;
        byte[] inputBytes = input.getBytes();
        String hashValue = "";
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(inputBytes);
        byte[] digestedBytes = messageDigest.digest();
        hashValue = bytesToHex(digestedBytes).toLowerCase();
        return hashValue;
    }
    
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

}
