/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class SendEmailServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOFactory daoFactory = (DAOFactory) request.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            response.sendError(500, "Impossible to access the database. Error retrieving DAO factory");
            return;
        }
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        String type = request.getParameter("med_type");
        System.out.println(type);
        String path = null, message = null, subject = null;
        if(type.equals("SPEC")){
            path = "/restricted/medicospecialista/home.html";
            subject = "Nuovo referto disponibile online";
            message = "Buongiorno,\nla informiamo che è disponibile un nuovo referto consultabile nel nostro portale web.\nBuona giornata.";
        }
        else{
            path = "/restricted/medicobase/pazienti.html";//TODO mettere poi path giusta
            subject = "Nuova prescrizione";
            message = "Buongiorno,\nla informiamo che è stata effettuata una nuova prescrizione da parte del suo medico di base.\nBuona giornata.";
        }
        
        String mail = (String) request.getSession().getAttribute("mail_paziente");
        System.out.println(mail);
        if (mail == null) {
            request.setAttribute("emailSent", false);
            request.setAttribute("emailError", "Message not set!");
            response.sendRedirect(response.encodeRedirectURL(contextPath + path));
            return;
        }

        StringBuilder plainTextMessageBuilder = new StringBuilder();
        plainTextMessageBuilder.append(message).append("\n");
        StringBuilder htmlMessageBuilder = new StringBuilder();
        message = message.replace(" ", "&nbsp;");
        message = message.replace("\n", "<br>");
        htmlMessageBuilder.append(message).append("<br>");
        final String host = getServletContext().getInitParameter("smtp-hostname");
        final String port = getServletContext().getInitParameter("smtp-port");
        final String username = getServletContext().getInitParameter("smtp-username");
        final String password = getServletContext().getInitParameter("smtp-password");
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");
        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
            
        });
        try {
            Multipart multipart = new MimeMultipart("alternative");
            
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText(plainTextMessageBuilder.toString());
            
            BodyPart messageBodyPart2 = new MimeBodyPart();
            messageBodyPart2.setContent(htmlMessageBuilder.toString(), "text/html; charset=utf-8");
            
            multipart.addBodyPart(messageBodyPart1);
            multipart.addBodyPart(messageBodyPart2);
            
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent(multipart);
            
            Transport.send(msg);
            
            request.setAttribute("emailSent", true);
            if (!response.isCommitted()) {
                response.sendRedirect(response.encodeRedirectURL(contextPath + path));
            }
        } catch (MessagingException | UnsupportedEncodingException me) {
            Logger.getLogger(getClass().getName()).severe(me.toString());
            
            request.setAttribute("emailSent", false);
            request.setAttribute("emailError", me.getMessage());
            if (!response.isCommitted()) {
                response.sendRedirect(response.encodeRedirectURL(contextPath + path));
            }
        }
    }

}
