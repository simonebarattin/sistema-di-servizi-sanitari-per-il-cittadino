/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.progetto_finale.persistence.entities.MedicoSpecialista;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author simon
 */
@WebServlet(name = "MSLogoutServlet", urlPatterns = {"/MSLogoutServlet"})
public class MSLogoutServlet extends HttpServlet {
    
    Cookie[] cookies;
    Cookie cookie;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        cookies = request.getCookies();
        for(int i=0; i<cookies.length; i++){
            cookie = cookies[i];
            if(!cookie.getName().equals("mb_mail") && !cookie.getName().equals("mb_password") && !cookie.getName().equals("ssp_mail") && !cookie.getName().equals("ssp_password") && !cookie.getName().equals("paz_mail") && !cookie.getName().equals("paz_password")){
                cookie.setValue("");
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }

        HttpSession session = request.getSession(false);
        if (session != null) {
            MedicoSpecialista mb = (MedicoSpecialista) session.getAttribute("medicospecialista");
            if (mb != null) {
                session.setAttribute("medicospecialista", null);
                session.invalidate();
                mb = null;
            }
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        if (!response.isCommitted()) {
            response.sendRedirect(response.encodeRedirectURL(contextPath + "specialista/loginspecialista.html"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}