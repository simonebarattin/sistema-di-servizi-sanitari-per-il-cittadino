/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.MedicoBaseDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class MBLoginServlet extends HttpServlet {
    
    private MedicoBaseDAO mbDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            mbDao = daoFactory.getDAO(MedicoBaseDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("pass");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        try{
            MedicoBase mb = mbDao.getByEmailAndPassword(email, password);
            if(mb == null){
                response.sendRedirect(response.encodeRedirectURL(contextPath + "base/loginbase.html"));
            }
            else{
                String[] check = request.getParameterValues("type");
                if(check!=null && check[0].equals("ricordami")){
                    Cookie pass = new Cookie("mb_password", password);
                    Cookie mail = new Cookie("mb_mail", email);
                    pass.setMaxAge(3600 * 1000 * 24 * 365);
                    mail.setMaxAge(3600 * 1000 * 24 * 365);
                    response.addCookie(mail);
                    response.addCookie(pass);
                }
                request.getSession().setAttribute("medicobase", mb);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/medicobase/base.handler"));
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
    }
    
}
