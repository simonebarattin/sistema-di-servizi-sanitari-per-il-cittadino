/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.SSPDAO;
import com.sip_project.progetto_finale.persistence.entities.SSP;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class SSPLoginServlet extends HttpServlet {
    
    private SSPDAO sspDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            sspDao = daoFactory.getDAO(SSPDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("pass");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        try{
            SSP ssp = sspDao.getByEmailAndPassword(email, password);
            if(ssp == null){
                response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/loginssp.html"));
            }
            else{
                String[] check = request.getParameterValues("type");
                if(check!=null && check[0].equals("ricordami")){
                    Cookie pass = new Cookie("ssp_password", password);
                    Cookie mail = new Cookie("ssp_mail", email);
                    pass.setMaxAge(3600 * 1000 * 24 * 365);
                    mail.setMaxAge(3600 * 1000 * 24 * 365);
                    response.addCookie(mail);
                    response.addCookie(pass);
                }
                Cookie temp = new Cookie("ssp_provincia",ssp.getProvincia());
                response.addCookie(temp);
                request.getSession().setAttribute("ssp", ssp);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/ssp/home.html"));
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
    }
}
