/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
/**
 *
 * @author simon
 */
public class XLSServlet extends HttpServlet {
    
    private String[] columns = {"ID Prescrizione", "Farmaco", "Medico di Base", "Paziente", "Data e Ora"};
    private List<PrescrizioneFarmaco> farmaci;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        farmaci = (List<PrescrizioneFarmaco>) request.getSession().getAttribute("farmaci");
        try {
            String filename = "C:\\Users\\simon\\Downloads\\report.xls" ;
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Report");  

            HSSFRow rowhead = sheet.createRow((short)0);
            rowhead.createCell(0).setCellValue("ID Prescrizione");
            rowhead.createCell(1).setCellValue("Farmaco");
            rowhead.createCell(2).setCellValue("Medicodi Base");
            rowhead.createCell(3).setCellValue("Paziente");
            rowhead.createCell(4).setCellValue("Data e Ora");
            
            CreationHelper createHelper = workbook.getCreationHelper();
            
            CellStyle dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
            
            int rowNum = 1;
            for(PrescrizioneFarmaco farmaco: farmaci) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(farmaco.getId_ric());
                row.createCell(1).setCellValue(farmaco.getNome());
                row.createCell(2).setCellValue(farmaco.getMedico());
                row.createCell(3).setCellValue(farmaco.getPaziente());
                Cell dateOfBirthCell = row.createCell(4);
                dateOfBirthCell.setCellValue(farmaco.getErog());
                dateOfBirthCell.setCellStyle(dateCellStyle);
            }
            for(int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }
            FileOutputStream fileOut = new FileOutputStream(filename);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
            System.out.println("Your excel file has been generated!");

        } catch ( Exception ex ) {
            System.out.println(ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        processRequest(request, response);
        response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/ssp/home.html"));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        processRequest(request, response);
        response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/ssp/home.html"));
    }
}
