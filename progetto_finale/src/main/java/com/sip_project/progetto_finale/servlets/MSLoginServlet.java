/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sip_project.progetto_finale.persistence.dao.MedicoSpecialistaDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoSpecialista;
import javax.servlet.http.Cookie;

/**
 *
 * @author simon
 */
public class MSLoginServlet extends HttpServlet {
    
    private MedicoSpecialistaDAO msDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            msDao = daoFactory.getDAO(MedicoSpecialistaDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("pass");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        try{
            MedicoSpecialista ms = msDao.getByEmailAndPassword(email, password);
            if(ms == null){
                response.sendRedirect(response.encodeRedirectURL(contextPath + "specialista/loginspecialista.html"));
            }
            else{
                String[] check = request.getParameterValues("type");
                if(check!=null && check[0].equals("ricordami")){
                    Cookie pass = new Cookie("ms_password", password);
                    Cookie mail = new Cookie("ms_mail", email);
                    pass.setMaxAge(3600 * 1000 * 24 * 365);
                    mail.setMaxAge(3600 * 1000 * 24 * 365);
                    response.addCookie(mail);
                    response.addCookie(pass);
                }
                request.getSession().setAttribute("medicospecialista", ms);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/medicospecialista/specialista.handler"));
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
    }

}
