/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import javax.servlet.http.HttpServlet;
import com.sip_project.progetto_finale.persistence.dao.MedicoSpecialistaDAO;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import com.sip_project.progetto_finale.persistence.dao.VisitaMSDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoSpecialista;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class MedicoSpecialistaServlet extends HttpServlet{
    
    private MedicoSpecialistaDAO msDao;
    private PazienteDAO pDao;
    private VisitaMSDAO vmsDao;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for storage system");
        }
        try {
            msDao = daoFactory.getDAO(MedicoSpecialistaDAO.class);
            pDao = daoFactory.getDAO(PazienteDAO.class);
            vmsDao = daoFactory.getDAO(VisitaMSDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        } 
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String command = req.getParameter("command");
            if(command == null)
                command = "PARCO";
        switch(command){
            case "PARCO":
                parcoPazienti(req, resp);
                break;
            case "LOAD":
                loadPatient(req,resp);
                break;
            case "LOAD_VISITA":
                loadVisita(req,resp);
                break;
            case "REPORT":
                loadReport(req,resp);
                break;
        }
    }
    
    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String command = req.getParameter("command");
            if(command == null)
                command = "PARCO";
            switch(command){
                case "PARCO":
                    parcoPazienti(req, resp);
                    break;
                case "VISITA":
                    visite(req, resp);
                    break;
                case "UPDATE":
                    updateVisita(req,resp);
                    break;
                case "TICKET":
                    erogaTicket(req, resp);
                    break;
                case "UPDATE_REPORT":
                    updateReport(req, resp);
                    break;
            }
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    

    private void parcoPazienti(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            List<Paziente> patients = msDao.getPatients();
            req.getSession().setAttribute("patients", patients);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicospecialista/pazienti.html");
            rd.forward(req, resp);
        } catch (SQLException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadPatient(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            String ssn = req.getParameter("pazienteSSN");
            Paziente p = (Paziente) pDao.getByPrimaryKey(ssn);
            List<VisitaMS> visite = (List<VisitaMS>) vmsDao.getBySSN(ssn);
            req.setAttribute("visitePaz", visite);
            req.setAttribute("schedaPaziente", p);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicospecialista/schedaPaziente.html");
            rd.forward(req, resp);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void visite(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        MedicoSpecialista ms = (MedicoSpecialista) req.getSession().getAttribute("medicospecialista");
        int id_ms = ms.getId();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            List<VisitaMS> visite = vmsDao.getAll(id_ms);
            Collections.sort(visite);
            req.getSession().setAttribute("visite", visite);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicospecialista/visite.html");
            rd.forward(req, resp);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadVisita(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            int id_vs = Integer.parseInt(req.getParameter("idVisita"));
            VisitaMS vms = vmsDao.getByPrimaryKey(id_vs);
            req.getSession().setAttribute("datiVisita", vms);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicospecialista/visite/update.html");
            rd.forward(req, resp);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateVisita(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            vmsDao.updateAnamnesi(req.getParameter("anamnesi"), Integer.parseInt(req.getParameter("id")));
            resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/restricted/medicospecialista/specialista.handler"));
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void erogaTicket(HttpServletRequest req, HttpServletResponse resp) {
        MedicoSpecialista ms = (MedicoSpecialista) req.getSession().getAttribute("medicospecialista");
        int id_ms = ms.getId();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            List<VisitaMS> visite = vmsDao.getVisiteCompletate(id_ms);
            List<VisitaMS> esami = vmsDao.getEsamiCompletati(id_ms);
            Collections.sort(visite);
            Collections.sort(esami);
            req.getSession().setAttribute("visite_ticket", visite);
            req.getSession().setAttribute("esami_ticket", esami);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicospecialista/erogaticket.html");
            rd.forward(req, resp);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateReport(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            vmsDao.updateReport(req.getParameter("risultato"), Integer.parseInt(req.getParameter("id_vs")));
            req.getSession().setAttribute("med_type", req.getParameter("med_type"));
            req.getSession().setAttribute("mail_paziente", req.getParameter("mail_paziente"));
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/email.handler");
            rd.forward(req, resp);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadReport(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            int id_vs = Integer.parseInt(req.getParameter("id_vs"));
            System.out.println("\n\n\n"+id_vs+"\n\n\n\n");
            VisitaMS vms = vmsDao.getByPrimaryKey(id_vs);
            //System.out.println("\n\n\n"+vms.getAnamnesi()+" "+vms.getId_vs()+"\n\n\n\n");
            req.getSession().setAttribute("datiVisita", vms);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicospecialista/updatereport.html");
            rd.forward(req, resp);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
