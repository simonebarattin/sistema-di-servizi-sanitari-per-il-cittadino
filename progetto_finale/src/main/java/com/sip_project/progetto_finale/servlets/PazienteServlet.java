/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import com.sip_project.progetto_finale.persistence.entities.Ticket;
import com.sip_project.progetto_finale.persistence.entities.VisitaMB;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import com.sip_project.progetto_finale.persistence.entities.VisitaSSP;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author simon
 */
public class PazienteServlet extends HttpServlet {
    
    private PazienteDAO pDao;
    private Cookie cookie = null;
    private Cookie[] cookies = null;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for storage system");
        }
        try {
            pDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        } 
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String command = request.getParameter("command");
        if(command == null)
            command = "NOTIFICA";
        switch(command){
            case "UPDATE":
                updateMedico(request,response);
                break;
            case "NOTIFICA":
                notifica(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String command = request.getParameter("command");
        if(command == null)
            command = "NOTIFICA";
        switch(command){
            case "CAMBIO":
                cambioMedico(request,response);
                break;
            case "RICETTE":
                listaRicette(request,response);
                break;
            case "VISITE":
                listaVisite(request,response);
                break;
            case "ESAMI":
                listaEsami(request,response);
                break;
            case "TICKET":
                listaTicket(request,response);
                break;
            default:
                cambiaFoto(request,response);
                break;
        }

    }

    private void cambioMedico(HttpServletRequest request, HttpServletResponse response) {
        cookies = request.getCookies();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            String provincia = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("provincia"))
                    provincia = cookie.getValue();
            }
            List<MedicoBase> medici = pDao.getMedici(provincia);
            request.getSession().setAttribute("medici", medici);
            //request.getSession().setAttribute("ssn", request.getParameter("ssn"));
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/paziente/cambiamedico.html");
            rd.forward(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateMedico(HttpServletRequest request, HttpServletResponse response) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            int id_mb = Integer.parseInt(request.getParameter("id_mb"));
            String ssn = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ssn"))
                    ssn = cookie.getValue();
            }
            Paziente p = pDao.updateMB(ssn, id_mb);
            request.getSession().setAttribute("paziente", p);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/paziente/home.html"));
        } catch (DAOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listaRicette(HttpServletRequest request, HttpServletResponse response) {
        cookies = request.getCookies();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try{
            String ssn = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ssn"))
                    ssn = cookie.getValue();
            }
            List<PrescrizioneFarmaco> farmaci = pDao.getFarmaci(ssn);
            request.getSession().setAttribute("ricette", farmaci);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/paziente/listaricette.html");
            rd.forward(request, response);            
        } catch (DAOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listaVisite(HttpServletRequest request, HttpServletResponse response) {
        cookies = request.getCookies();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try{
            String ssn = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ssn"))
                    ssn = cookie.getValue();
            }
            List<VisitaMB> visiteBComp = pDao.getVisiteBComplete(ssn);
            List<VisitaMB> visiteBNComp = pDao.getVisiteBNonComplete(ssn);
            List<VisitaMS> visiteSComp = pDao.getVisiteSComplete(ssn);
            List<VisitaMS> visiteSNComp = pDao.getVisiteSNonComplete(ssn);
            request.getSession().setAttribute("visiteBComp", visiteBComp);
            request.getSession().setAttribute("visiteBNComp", visiteBNComp);
            request.getSession().setAttribute("visiteSComp", visiteSComp);
            request.getSession().setAttribute("visiteSNComp", visiteSNComp);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/paziente/listavisite.html");
            rd.forward(request, response);            
        } catch (DAOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listaEsami(HttpServletRequest request, HttpServletResponse response) {
        cookies = request.getCookies();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try{
            String ssn = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ssn"))
                    ssn = cookie.getValue();
            }
            List<VisitaSSP> esamiSSPComp = pDao.getEsamiSSPComplete(ssn);
            List<VisitaSSP> esamiSSPNComp = pDao.getEsamiSSPNonComplete(ssn);
            List<VisitaMS> esamiSComp = pDao.getEsamiSCompleti(ssn);
            List<VisitaMS> esamiSNComp = pDao.getEsamiSNCompleti(ssn);
            request.getSession().setAttribute("esamiSSPComp", esamiSSPComp);
            request.getSession().setAttribute("esamiSSPNComp", esamiSSPNComp);
            request.getSession().setAttribute("esamiSComp", esamiSComp);
            request.getSession().setAttribute("esamiSNComp", esamiSNComp);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/paziente/listaesami.html");
            rd.forward(request, response); 
        }catch(DAOException e){
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, e);
        } catch (ServletException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listaTicket(HttpServletRequest request, HttpServletResponse response) {
        cookies = request.getCookies();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try{
            String ssn = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ssn"))
                    ssn = cookie.getValue();
            }
            List<Ticket> tickets = pDao.getTickets(ssn);
            request.getSession().setAttribute("tickets",tickets);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/paziente/listaticket.html");
            rd.forward(request, response);
        }catch(DAOException e){
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, e);
        } catch (ServletException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void notifica(HttpServletRequest request, HttpServletResponse response) {
        cookies = request.getCookies();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try{
            String ssn = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ssn"))
                    ssn = cookie.getValue();
            }
            List<VisitaMB> visiteBNComp = pDao.getVisiteBNonComplete(ssn);
            VisitaMB visitaMB = null;
            if(! visiteBNComp.isEmpty())
                visitaMB = visiteBNComp.get(0);
            List<VisitaMS> visiteSNComp = pDao.getVisiteSNonComplete(ssn);
            VisitaMS visitaMS = null;
            if(! visiteSNComp.isEmpty())
                visitaMS = visiteSNComp.get(0);
            List<VisitaSSP> esamiSSPNComp = pDao.getEsamiSSPNonComplete(ssn);
            VisitaSSP esameSSP = null;
            if(! esamiSSPNComp.isEmpty())
                esameSSP = esamiSSPNComp.get(0);
            List<VisitaMS> esamiSNComp = pDao.getEsamiSNCompleti(ssn);
            VisitaMS esameMS = null;
            if(! esamiSNComp.isEmpty())
                esameMS = esamiSNComp.get(0);
            request.getSession().setAttribute("visitaMB", visitaMB);
            request.getSession().setAttribute("visitaMS", visitaMS);
            request.getSession().setAttribute("esameSSP", esameSSP);
            request.getSession().setAttribute("esameMS", esameMS);
            RequestDispatcher rd = request.getRequestDispatcher("/restricted/paziente/home.html");
            rd.forward(request, response);    
        }catch(DAOException e){
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, e);
        } catch (ServletException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cambiaFoto(HttpServletRequest request, HttpServletResponse response) {
        cookies = request.getCookies();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            String ssn = null;
            for(int i=0;i<cookies.length;i++){
                cookie = cookies[i];
                if(cookie.getName().equals("ssn"))
                    ssn = cookie.getValue();
            }
            ServletFileUpload fileUpload = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> file = fileUpload.parseRequest(request);
            FileItem item = file.get(0);
            String name = item.getName();
            String uploadPath = "C:/Users/simon/Documents/web/sistema-di-servizi-sanitari-per-il-cittadino/progetto_finale/src/main/webapp/images/avatars/" + name;
            item.write(new File(uploadPath)); 
            Paziente p = pDao.updatePhoto(name, ssn);
            request.getSession().setAttribute("paziente", p);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "/restricted/paziente/home.html"));
        } catch (FileUploadException ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
