/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import com.sip_project.progetto_finale.persistence.entities.Report;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import javax.servlet.http.Cookie;


/**
 *
 * @author simon
 */
public class PDFServlet extends HttpServlet{
    
    private PazienteDAO pDao;
    private Cookie[] cookies;
    private Cookie cookie;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for storage system");
        }
        try {
            pDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for patient storage system", ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pdfFolder = getServletContext().getInitParameter("pdfFolder");
        if (pdfFolder == null) {
            throw new ServletException("PDFs folder not configured");
        }
        cookies = request.getCookies();
        String ssn = null;
        for(int i=0;i<cookies.length;i++){
            cookie = cookies[i];
            if(cookie.getName().equals("ssn"))
                ssn = cookie.getValue();
        }
        
        pdfFolder = getServletContext().getRealPath(pdfFolder);

        Integer reportId = null;
        String type = null;
        try {
            reportId = Integer.valueOf(request.getParameter("id"));
            type = request.getParameter("type");
            System.out.println("\n\n\n"+reportId+"\n\n\n");
        } catch (NumberFormatException | NullPointerException ex) {
            throw new ServletException("No valid report id");
        }
        Report rep = null;
        int id_tick =Integer.parseInt(request.getParameter("id_tick"));
        try {
            rep = pDao.getReportByPrimaryKey(reportId,type);
            pDao.updateTicket(id_tick ,type);
        }catch (DAOException ex) {
            Logger.getLogger(PDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);

            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
                contents.beginText(); 
                contents.setLeading(14.5f);
                contents.newLineAtOffset(25, 700);
                contents.setFont( PDType1Font.TIMES_ROMAN, 16 );

                String text1 = "Report della visita/esame del paziente con codice fiscale" + ssn;
                String text2 = "Identificativo report: "+rep.getId_rep()+", erogato in data "+rep.getErog()+".";
                String text3 = "Esame svolto: "+rep.getEsame()+".";
                String text4 = "Risultato dell\' esame: "+rep.getRisultato()+".";
                String text5 = "Valore del ticket da pagare: "+request.getParameter("value");

                //Adding text in the form of string
                contents.showText(text1);
                contents.newLine();
                contents.newLine();
                contents.showText(text2);
                contents.newLine();
                contents.newLine();
                contents.showText(text3);
                contents.newLine();
                contents.newLine();
                contents.showText(text4);
                contents.newLine();
                contents.newLine();
                contents.showText(text5);
                //Ending the content stream
                contents.endText();
                contents.close();
            }
            doc.save(pdfFolder +" "+ ssn + ".pdf");

            response.setHeader("Content-disposition", "attachment; filename='report.pdf'");
            response.setContentType("application/pdf");
            doc.save(response.getOutputStream());
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        System.out.println(contextPath);

        response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/paziente/home.html"));
    }    
    
}
