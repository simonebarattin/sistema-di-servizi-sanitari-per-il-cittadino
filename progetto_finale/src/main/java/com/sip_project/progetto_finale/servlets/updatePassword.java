/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.MedicoBaseDAO;
import com.sip_project.progetto_finale.persistence.dao.MedicoSpecialistaDAO;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import com.sip_project.progetto_finale.persistence.dao.SSPDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class updatePassword extends HttpServlet {
    
    private MedicoBaseDAO mbDao;
    private MedicoSpecialistaDAO msDao;
    private PazienteDAO pDao;
    private SSPDAO sspDao;
    private Cookie cookie = null;
    private Cookie[] cookies = null;
    private Encrypt enc;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for storage system");
        }
        try {
            sspDao = daoFactory.getDAO(SSPDAO.class);
            mbDao = daoFactory.getDAO(MedicoBaseDAO.class);
            msDao = daoFactory.getDAO(MedicoSpecialistaDAO.class);
            pDao = daoFactory.getDAO(PazienteDAO.class);
            enc = new Encrypt();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        } 
    }   

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();        
        String type = request.getParameter("type");
        String mail = request.getParameter("mail");
        String pass = request.getParameter("new_pass");
        String salt = null, hash = null, path = null;
        try {
            switch(type){
                case "paz":
                    salt = pDao.getSaltByEmail(mail);
                    hash = enc.getHash(pass, salt);
                    pDao.updateHash(hash,mail);
                    path = "/paziente/loginpaziente.html";
                    break;
                case "spec":
                    salt = msDao.getSaltByEmail(mail);
                    hash = enc.getHash(pass, salt);
                    msDao.updateHash(hash,mail);
                    path = "/specialista/loginspecialista.html";
                    break;
                case "base":
                    salt = mbDao.getSaltByEmail(mail);
                    hash = enc.getHash(pass, salt);
                    mbDao.updateHash(hash,mail);
                    path="/base/loginbase.html";
                    break;
                case "ssp":
                    salt = sspDao.getSaltByEmail(mail);
                    hash = enc.getHash(pass, salt);
                    sspDao.updateHash(hash,mail);
                    path = "/ssp/loginssp.html";
                    break;
            }
        } catch (DAOException ex) {
            Logger.getLogger(updatePassword.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(updatePassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.getSession().setAttribute("cambioPass", true);
        response.sendRedirect(response.encodeRedirectURL(contextPath + path));
    }

}
