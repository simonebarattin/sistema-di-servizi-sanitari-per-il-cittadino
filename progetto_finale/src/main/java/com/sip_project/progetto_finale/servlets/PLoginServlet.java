/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author simon
 */
public class PLoginServlet extends HttpServlet {
    
    private PazienteDAO pDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            pDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("pass");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        try{
            Paziente p = pDao.getByEmailAndPassword(email, password);
            if(p == null){
                response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/loginpaziente.html"));
            }
            else{
                String[] check = request.getParameterValues("type");
                if(check!=null && check[0].equals("ricordami")){
                    Cookie pass = new Cookie("paz_password", password);
                    Cookie mail = new Cookie("paz_mail", email);
                    pass.setMaxAge(3600 * 1000 * 24 * 365);
                    mail.setMaxAge(3600 * 1000 * 24 * 365);
                    response.addCookie(mail);
                    response.addCookie(pass);
                }
                Cookie id = new Cookie("ssn",p.getSSN());
                id.setMaxAge(60*60*24);
                response.addCookie(id);
                Cookie provincia = new Cookie("provincia", p.getProvincia());
                provincia.setMaxAge(60*60*24);
                response.addCookie(provincia);
                request.getSession().setAttribute("paziente", p);
                response.sendRedirect(contextPath + "restricted/paziente/paziente.handler");                
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
    }

}
