/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.factories.DAOFactory;
import com.sip_project.progetto_finale.persistence.dao.EsameDAO;
import com.sip_project.progetto_finale.persistence.dao.FarmaciDAO;
import javax.servlet.http.HttpServlet;
import com.sip_project.progetto_finale.persistence.dao.MedicoBaseDAO;
import com.sip_project.progetto_finale.persistence.dao.MedicoSpecialistaDAO;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import com.sip_project.progetto_finale.persistence.dao.VisitaMBDAO;
import com.sip_project.progetto_finale.persistence.dao.VisitaMSDAO;
import com.sip_project.progetto_finale.persistence.entities.Esame;
import com.sip_project.progetto_finale.persistence.entities.Farmaci;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.MedicoSpecialista;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import com.sip_project.progetto_finale.persistence.entities.Report;
import com.sip_project.progetto_finale.persistence.entities.VisitaMB;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author simon
 */
public class MedicoBaseServlet extends HttpServlet {

    private MedicoBaseDAO mbDao;
    private PazienteDAO pDao;
    private VisitaMBDAO vmbDao;
    private Cookie cmb;
    private EsameDAO emb;
    private MedicoSpecialistaDAO msDao;
    private FarmaciDAO farm;
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for storage system");
        }
        try {
            mbDao = daoFactory.getDAO(MedicoBaseDAO.class);
            pDao = daoFactory.getDAO(PazienteDAO.class);
            vmbDao = daoFactory.getDAO(VisitaMBDAO.class);
            emb = daoFactory.getDAO(EsameDAO.class);
            msDao = daoFactory.getDAO(MedicoSpecialistaDAO.class);
            farm=daoFactory.getDAO(FarmaciDAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        } 
    }

    

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String command = req.getParameter("command");
            if(command == null)
                command = "PARCO";
            switch(command){
                case "PARCO":
                    parcoPazienti(req, resp);
                    break;
                case "LOAD":
                    loadPatient(req,resp);
                    break;
                case "PRESCRIZIONI":
                    loadPresc(req,resp);
                    break;
                case "REPORT":
                    loadReport(req,resp);
                    break;
                       
                    /* case "LOAD_VISITA":
                    loadVisita(req,resp);
                    break;*/
            }
        } catch (DAOException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //try {
            String command = req.getParameter("command");
            if(command == null)
                command = "PARCO";
            switch(command){
                case "PARCO":
                    parcoPazienti(req, resp);
                    break;
                case "ESAME":
                    loadVisitaSpec(req,resp);
                    break;
                case "SSP":
                    loadVisitaSsp(req,resp);
                    break;
                case "FARMACI":
                    loadFarmaci(req,resp);
                    break;
                   
               /* case "VISITA":
                    visite(req, resp);
                    break;
                case "UPDATE":
                    System.out.println(req.getParameter("id"));
                    updateVisita(req,resp);
                    break;
            */
                case "SUBMITspec":
                {
                    try {
                        submitErogazioneSpec(req, resp);
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                    break;
                     case "SUBMITssp":
                        submitErogazioneSsp(req, resp);
                    
               
                    break;
                    case "SUBMITfarm":
               
                   
                {
                    try {
                        submitErogazioneFarm(req, resp);
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                    
               
                    break;


            }
        /*} catch (DAOException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
    }    

    private void parcoPazienti(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        List<Paziente> patients= new ArrayList();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            int mbid=0;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
            }
            //System.out.println("\n\n\n\n\n\n"+mbid+"\n\n\n\n\n\n");
            patients = mbDao.getPatients(mbid);
            req.getSession().setAttribute("patients", patients);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicobase/pazienti.html");
            rd.forward(req, resp);
            
        } catch (SQLException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadPatient(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            String ssn = req.getParameter("pazienteSSN");
            Paziente p = (Paziente) pDao.getByPrimaryKey(ssn);
            List<VisitaMB> visite = (List<VisitaMB>) vmbDao.getBySSN(ssn);
            Collections.sort(visite);
            VisitaMB temp=new VisitaMB();
            if(!visite.isEmpty()){
                temp= visite.get(visite.size()-1);
                
            
            }
            //System.out.println("\n\n\n\n\n\n"+temp.getScadenza()+"\n\n\n\n\n\n\n\n");
            req.setAttribute("visitePaz", temp );
            req.setAttribute("schedaPaziente", p);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicobase/schedaPazienteB.html");
            rd.forward(req, resp);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadVisitaSpec(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            List <Esame> esami= emb.getAllSpec();
            List<MedicoSpecialista> ms= msDao.getAll();
            int mbid=0;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
                
            }
            //System.out.println("\n\n\n\n\n\n"+mbid+"\n\n\n\n\n\n");
            List <Paziente> patients = mbDao.getPatients(mbid);//null
            
            req.setAttribute("esami", esami );
            req.setAttribute("patients", patients );
            req.setAttribute("ms", ms);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicobase/esame.html");
            rd.forward(req, resp);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadVisitaSsp(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            List <Esame> esami= emb.getAllSsp();
            //List<MedicoSpecialista> ms= msDao.getAll();
            int mbid=0;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
                
            }
            List <Paziente> patients = mbDao.getPatients(mbid);//null
            
            req.setAttribute("esami", esami );
            req.setAttribute("patients", patients );
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicobase/esamessp.html");
            rd.forward(req, resp);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void submitErogazioneSpec(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
    
         String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        int mbid=0;
        String provincia = null;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                if(cmb[i].getName().equals("provincia"))
                    provincia=cmb[i].getValue();
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
            }
        String form1= (String) req.getParameter("form1");//id visita
        //String form2= req.getParameter("form2");// ssn
        String spec= req.getParameter("form3");//medico 
        String data= req.getParameter("vdate");//data
       
        emb.esameSpec(data, 0, 0, req.getParameter("form2").substring(0, 16), Integer.parseInt(spec), mbid, Integer.parseInt(form1.substring(0,1)));
        req.getSession().setAttribute("med_type", req.getParameter("med_type"));
        req.getSession().setAttribute("mail_paziente", req.getParameter("form2").substring(17).trim());
        RequestDispatcher rd = req.getRequestDispatcher("/restricted/email.handler");
        rd.forward(req, resp);
        /*if (form1.substring(2).equals("n")){
            int idssp=emb.getFromProvicia(provincia);
         
            emb.esameSsn(data, 0, 0, form2, idssp, Integer.parseInt(form1.substring(0,1)));
        }*/
    }

 private void submitErogazioneSsp(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            int mbid=0;
            String provincia = null;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                if(cmb[i].getName().equals("provincia"))
                    provincia=cmb[i].getValue();
                
            }
            int idssp=emb.getFromProvicia(provincia);

            emb.esameSsn(req.getParameter("vdate"), 0, 0, req.getParameter("form2").substring(0, 16), idssp, Integer.parseInt(req.getParameter("form1").substring(0,1)));
            req.getSession().setAttribute("med_type", req.getParameter("med_type"));
            req.getSession().setAttribute("mail_paziente", req.getParameter("form2").substring(17).trim());
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/email.handler");
            rd.forward(req, resp);

        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadFarmaci(HttpServletRequest req, HttpServletResponse resp) {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            List <Farmaci> farmaci= farm.getAll();
            //List<MedicoSpecialista> ms= msDao.getAll();
            int mbid=0;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
                
            }
            //System.out.println("\n\n\n\n\n\n"+mbid+"\n\n\n\n\n\n");
            List <Paziente> patients = mbDao.getPatients(mbid);//null
            
            req.setAttribute("farmaci", farmaci );
            req.setAttribute("patients", patients );
           // req.setAttribute("ms", ms);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicobase/prescrizionifarmaci.html");
            rd.forward(req, resp);
        } catch (DAOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MedicoBaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     private void submitErogazioneFarm(HttpServletRequest req, HttpServletResponse resp) throws DAOException, ServletException, IOException {
    
         String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        int mbid=0;
        String provincia = null;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                if(cmb[i].getName().equals("provincia"))
                    provincia=cmb[i].getValue();
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
            }
        String form1= (String) req.getParameter("form1");//id farmaco
        //String form2= req.getParameter("form2");// ssn
        String data= req.getParameter("vdate");//data
        
        farm.prescFarm(req.getParameter("form2").substring(0, 16), Integer.parseInt(form1), data, 0);
        req.getSession().setAttribute("med_type", req.getParameter("med_type"));
        req.getSession().setAttribute("mail_paziente", req.getParameter("form2").substring(17).trim());
        RequestDispatcher rd = req.getRequestDispatcher("/restricted/email.handler");
        rd.forward(req, resp);
        
    }

    private void loadPresc(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        String contextPath = getServletContext().getContextPath();
        List<PrescrizioneFarmaco> presc= new ArrayList();
       
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            String ssn = req.getParameter("pazienteSSN");
            int mbid=0;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
            }
            //System.out.println("\n\n\n\n\n\n"+mbid+"\n\n\n\n\n\n");
            presc= farm.getPresc(ssn, mbid);
              
            req.getSession().setAttribute("presc", presc);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicobase/prescrizioni.html");
            rd.forward(req, resp);
            
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadReport(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
     String contextPath = getServletContext().getContextPath();
        List<Report> report= new ArrayList();
       
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            String ssn = req.getParameter("pazienteSSN");
            int mbid=0;
            Cookie [] cmb= req.getCookies();
            for (int i=0; i< cmb.length; i++)
            {
                if (cmb[i].getName().equals("id_mb"))
                 mbid=Integer.parseInt(cmb[i].getValue());
                /*System.out.println(cmb[i].getValue());
                System.out.println(cmb[i].getName());*/
            }
            //System.out.println("\n\n\n\n\n\n"+mbid+"\n\n\n\n\n\n");
            List<VisitaMS> vms=new ArrayList<>();
            report=mbDao.getReportS(mbid, ssn,vms);
              
            req.getSession().setAttribute("report", report);
            req.getSession().setAttribute("esame", vms);
            RequestDispatcher rd = req.getRequestDispatcher("/restricted/medicobase/report.html");
            rd.forward(req, resp);
            
        } catch (IOException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(MedicoSpecialistaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
