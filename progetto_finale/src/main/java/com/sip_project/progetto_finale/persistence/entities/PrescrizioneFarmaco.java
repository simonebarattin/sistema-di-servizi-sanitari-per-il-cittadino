/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

import java.sql.Date;

/**
 *
 * @author simon
 */
public class PrescrizioneFarmaco {
    
    private int id_ric,id_farm,id_mb;
    private boolean ritirato;
    private String ssn,nome,paziente,medico;
    private Date erog;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    private Date data_erog;

    public int getId_ric() {
        return id_ric;
    }

    public void setId_ric(int id_ric) {
        this.id_ric = id_ric;
    }

    public int getId_farm() {
        return id_farm;
    }

    public void setId_farm(int id_farm) {
        this.id_farm = id_farm;
    }

    public boolean isRitirato() {
        return ritirato;
    }

    public void setRitirato(boolean ritirato) {
        this.ritirato = ritirato;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }  

    public int getId_mb() {
        return id_mb;
    }

    public void setId_mb(int id_mb) {
        this.id_mb = id_mb;
    }

    public Date getErog() {
        return erog;
    }

    public void setErog(Date erog) {
        this.erog = erog;
    }

    public String getPaziente() {
        return paziente;
    }

    public void setPaziente(String paziente) {
        this.paziente = paziente;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }    
}
