/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.Farmaci;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import java.util.List;
/**
 *
 * @author pietr
 */
public interface FarmaciDAO extends DAO<Farmaci, Integer> {
     public Farmaci getByType(String farmaco) throws DAOException;
   public List <PrescrizioneFarmaco> getPresc(String ssn, int id_mb) throws DAOException;
    public void prescFarm( String ssn, int id_farm, String data, int ritirato ) throws DAOException;
    public int getFromId(int id)throws DAOException;
    
}
