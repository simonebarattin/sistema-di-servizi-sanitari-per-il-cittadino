/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

/**
 *
 * @author simon
 */
public class SSP {
    
    private int id_ssp;
    private String provincia;

    public int getId_ssp() {
        return id_ssp;
    }

    public void setId_ssp(int id_ssp) {
        this.id_ssp = id_ssp;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }    
}
