/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import java.util.List;

/**
 *
 * @author simon
 */
public interface VisitaMSDAO extends DAO<VisitaMS, Integer>{
    
    public List<VisitaMS> getBySSN(String ssn) throws DAOException;
    
    public void updateAnamnesi(String anamnesi, int id_vs) throws DAOException;

    public List<VisitaMS> getAll(int id_ms);
    
    public List<VisitaMS> getVisiteCompletate(int id_ms) throws DAOException;
    
    public List<VisitaMS> getEsamiCompletati(int id_ms) throws DAOException;    

    public void updateReport(String parameter, int parseInt) throws DAOException;
}
