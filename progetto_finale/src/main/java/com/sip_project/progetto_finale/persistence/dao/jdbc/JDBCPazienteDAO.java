/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.PazienteDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import com.sip_project.progetto_finale.persistence.entities.Report;
import com.sip_project.progetto_finale.persistence.entities.Ticket;
import com.sip_project.progetto_finale.persistence.entities.VisitaMB;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import com.sip_project.progetto_finale.persistence.entities.VisitaSSP;
import com.sip_project.progetto_finale.servlets.Encrypt;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

/**
 *
 * @author simon
 */
public class JDBCPazienteDAO extends JDBCDAO<Paziente, String> implements PazienteDAO{
    
    public JDBCPazienteDAO(Connection con){
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Paziente> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Paziente getByEmailAndPassword(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) {
            throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));
        }
        
        try (PreparedStatement ps = CON.prepareStatement("SELECT * FROM loginutente WHERE email=?")) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String ssn = rs.getString("ssn");
            String salt = rs.getString("salt");
            String hash = rs.getString("hash");
            Encrypt enc = new Encrypt();
            String res = "";
            try {
                res = enc.getHash(password, salt);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(JDBCMedicoBaseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.close();
            rs.close();
            if(res.equals(hash)){
                Paziente p = getByPrimaryKey(ssn);
                return p;
            }
            return null;
        } catch (SQLException ex) {
            return null;
        } 
    }

    @Override
    public Paziente update(Paziente arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Paziente getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM paziente WHERE ssn=?");){
            ps.setString(1, primaryKey);
            try(ResultSet rs = ps.executeQuery();){
                rs.next();
                Paziente temp = new Paziente();
                temp.setSSN(rs.getString("ssn"));
                temp.setNome(rs.getString("nome"));
                temp.setCognome(rs.getString("cognome"));
                temp.setEmail(rs.getString("email"));
                temp.setSex(rs.getString("sesso").charAt(0));
                temp.setData(rs.getDate("datanascita"));
                temp.setFoto(rs.getString("foto_path"));
                temp.setProvincia(rs.getString("sp"));
                int id_mb = rs.getInt("id_mb");
                MedicoBase mb = new MedicoBase();
                PreparedStatement ps2 = CON.prepareStatement("SELECT * FROM medicobase WHERE id_mb=?");
                ps2.setInt(1,id_mb);
                ResultSet rs2 = ps2.executeQuery();
                rs2.next();
                mb.setId(rs2.getInt("id_mb"));
                mb.setNome(rs2.getString("nome"));
                mb.setCognome(rs2.getString("cognome"));
                mb.setProvincia(rs2.getString("sp"));
                mb.setCitta(rs2.getString("citta"));
                rs2.close();
                ps2.close();
                temp.setMb(mb);
                rs.close();
                ps.close();
                return temp;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the user for the passed primary key", e);
        }
    }

    @Override
    public Paziente updateMB(String ssn, int id_mb) throws DAOException {
        if (ssn==null || id_mb==0) {
            throw new DAOException("primaryKey is null");
        }
        Paziente p = null;
        try(PreparedStatement ps = CON.prepareStatement("UPDATE paziente SET id_mb=? WHERE ssn=?")){
            ps.setInt(1, id_mb);
            ps.setString(2,ssn); 
            ps.executeUpdate();
            ps.close();
        }catch(SQLException e){
            throw new DAOException("Impossible to get the user for the passed primary key", e);
        }
        p = this.getByPrimaryKey(ssn);
        return p;
    }

    @Override
    public List<MedicoBase> getMedici(String provincia) throws DAOException {
        List<MedicoBase> medici = new ArrayList<>();
        if(provincia == null){
            throw new DAOException("provincia is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM medicobase where sp=?")){
            ps.setString(1,provincia);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    MedicoBase mb=new MedicoBase();
                    mb.setId(rs.getInt("id_mb"));
                    mb.setNome(rs.getString("nome"));
                    mb.setCognome(rs.getString("cognome"));
                    mb.setProvincia(rs.getString("sp"));
                    mb.setCitta(rs.getString("citta"));
                    medici.add(mb);
                }
                ps.close();
                rs.close();
            }
            return medici;
        }catch(SQLException e){
            throw new DAOException("Impossible to get the user for the passed primary key", e);
        }
    }

    @Override
    public List<PrescrizioneFarmaco> getFarmaci(String ssn) throws DAOException {
        List<PrescrizioneFarmaco> farmaci = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT prescrizionefarmaco.*,paziente.id_mb,farmaco.nome FROM farmaco JOIN prescrizionefarmaco ON farmaco.id_farm=prescrizionefarmaco.id_farm JOIN paziente ON prescrizionefarmaco.SSN=paziente.ssn WHERE paziente.ssn=?")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    PrescrizioneFarmaco temp = new PrescrizioneFarmaco();
                    temp.setId_ric(rs.getInt("prescrizionefarmaco.id_ric"));
                    temp.setId_farm(rs.getInt("prescrizionefarmaco.id_farm"));
                    temp.setNome(rs.getString("farmaco.nome"));
                    temp.setSsn(rs.getString("prescrizionefarmaco.ssn"));
                    temp.setRitirato(rs.getBoolean("prescrizionefarmaco.ritirato"));
                    temp.setErog(rs.getDate("prescrizionefarmaco.data_erog"));
                    temp.setId_mb(rs.getInt("paziente.id_mb"));
                    farmaci.add(temp);
                }
                rs.close();
                ps.close();
            }
            return farmaci;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaMB> getVisiteBComplete(String ssn) throws DAOException {
        List<VisitaMB> visite = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicobase JOIN medicobase ON visitamedicobase.id_mb=medicobase.id_mb WHERE ssn=? AND completato=1")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMB temp = new VisitaMB();
                    temp.setId_vb(rs.getInt("id_vb"));
                    temp.setId_mb(rs.getInt("id_mb"));
                    temp.setAnamnesi(rs.getString("anamnesi"));
                    temp.setScadenza(rs.getDate("scadenza"));
                    temp.setCompleto(rs.getBoolean("completato"));
                    temp.setSsn(rs.getString("ssn"));
                    temp.setMb(rs.getString("medicobase.cognome")+rs.getString("medicobase.nome"));
                    visite.add(temp);
                }
                Collections.sort(visite);
                rs.close();
                ps.close();
            }
            return visite;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaMB> getVisiteBNonComplete(String ssn) throws DAOException {
        List<VisitaMB> visite = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicobase JOIN medicobase ON visitamedicobase.id_mb=medicobase.id_mb WHERE ssn=? AND completato=0")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMB temp = new VisitaMB();
                    temp.setId_vb(rs.getInt("visitamedicobase.id_vb"));
                    temp.setId_mb(rs.getInt("visitamedicobase.id_mb"));
                    temp.setAnamnesi(rs.getString("visitamedicobase.anamnesi"));
                    temp.setScadenza(rs.getDate("visitamedicobase.scadenza"));
                    temp.setCompleto(rs.getBoolean("visitamedicobase.completato"));
                    temp.setSsn(rs.getString("visitamedicobase.ssn"));
                    temp.setMb(rs.getString("medicobase.cognome")+rs.getString("medicobase.nome"));
                    visite.add(temp);
                }
                if(!visite.isEmpty())
                    Collections.sort(visite);
                rs.close();
                ps.close();
            }
            return visite;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaSSP> getEsamiSSPComplete(String ssn) throws DAOException {
        List<VisitaSSP> visite = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitassp JOIN paziente ON visitassp.ssn=paziente.ssn JOIN esame ON visitassp.id_es=esame.id_es WHERE paziente.ssn=? AND completato=1")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaSSP temp = new VisitaSSP();
                    temp.setId_vssp(rs.getInt("visitassp.id_vssp"));
                    temp.setId_ssp(rs.getInt("visitassp.id_ssp"));
                    temp.setScadenza(rs.getDate("visitassp.scadenza"));
                    temp.setCompleto(rs.getBoolean("visitassp.completato"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    paz.setEmail(rs.getString("paziente.email"));
                    temp.setPaz(paz);
                    temp.setEsame(rs.getString("esame.tipo"));
                    visite.add(temp);
                }
                if(!visite.isEmpty())
                    Collections.sort(visite);
                rs.close();
                ps.close();
            }
            return visite;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaSSP> getEsamiSSPNonComplete(String ssn) throws DAOException {
        List<VisitaSSP> visite = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitassp JOIN paziente ON visitassp.ssn=paziente.ssn JOIN esame ON visitassp.id_es=esame.id_es WHERE paziente.ssn=? AND completato=0")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaSSP temp = new VisitaSSP();
                    temp.setId_vssp(rs.getInt("visitassp.id_vssp"));
                    temp.setId_ssp(rs.getInt("visitassp.id_ssp"));
                    temp.setScadenza(rs.getDate("visitassp.scadenza"));
                    temp.setCompleto(rs.getBoolean("visitassp.completato"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    paz.setEmail(rs.getString("paziente.email"));
                    temp.setPaz(paz);
                    temp.setEsame(rs.getString("esame.tipo"));
                    visite.add(temp);
                }
                if(!visite.isEmpty())
                    Collections.sort(visite);
                rs.close();
                ps.close();
            }
            return visite;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaMS> getVisiteSComplete(String ssn) throws DAOException {
        List<VisitaMS> visite = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN medicospecialista ON visitamedicospec.id_ms=medicospecialista.id_ms WHERE ssn=? AND completato=1 AND id_es IS NULL")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("visitamedicospec.id_vs"));
                    temp.setAnamnesi(rs.getString("visitamedicospec.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicospec.completato"));
                    temp.setErog(rs.getDate("visitamedicospec.scadenza"));
                    temp.setId_ms(rs.getInt("visitamedicospec.id_ms"));
                    temp.setEsame(null);
                    temp.setMs(rs.getString("medicospecialista.cognome")+rs.getString("medicospecialista.nome"));
                    visite.add(temp);
                }
                if(!visite.isEmpty())
                    Collections.sort(visite);
                rs.close();
                ps.close();
            }
            return visite;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaMS> getVisiteSNonComplete(String ssn) throws DAOException {
        List<VisitaMS> visite = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN medicospecialista ON visitamedicospec.id_ms=medicospecialista.id_ms WHERE ssn=? AND completato=0 AND id_es IS NULL")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("id_vs"));
                    temp.setAnamnesi(rs.getString("anamnesi"));
                    temp.setCompleto(rs.getBoolean("completato"));
                    temp.setErog(rs.getDate("scadenza"));
                    temp.setId_ms(rs.getInt("id_ms"));
                    temp.setEsame(null);
                    temp.setMs(rs.getString("medicospecialista.cognome")+rs.getString("medicospecialista.nome"));
                    visite.add(temp);
                }
                if(!visite.isEmpty())
                    Collections.sort(visite);
                rs.close();
                ps.close();
            }
            return visite;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaMS> getEsamiSCompleti(String ssn) throws DAOException {
        List<VisitaMS> esami = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN medicospecialista ON visitamedicospec.id_ms=medicospecialista.id_ms WHERE ssn=? AND completato=1 AND id_es IS NOT NULL")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("id_vs"));
                    temp.setAnamnesi(rs.getString("anamnesi"));
                    temp.setCompleto(rs.getBoolean("completato"));
                    temp.setErog(rs.getDate("scadenza"));
                    temp.setId_ms(rs.getInt("id_ms"));
                    int ides = rs.getInt("id_es");
                    PreparedStatement ps2 = CON.prepareStatement("SELECT * FROM esame WHERE id_es=?");
                    ps2.setInt(1,ides);
                    ResultSet rs2 = ps2.executeQuery();
                    rs2.next();
                    temp.setEsame(rs2.getString("tipo"));
                    temp.setMs(rs.getString("medicospecialista.cognome")+rs.getString("medicospecialista.nome"));
                    rs2.close();
                    ps2.close();
                    esami.add(temp);
                }
                if(!esami.isEmpty())
                    Collections.sort(esami);
                rs.close();
                ps.close();
            }
            return esami;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<VisitaMS> getEsamiSNCompleti(String ssn) throws DAOException {
        List<VisitaMS> esami = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN medicospecialista ON visitamedicospec.id_ms=medicospecialista.id_ms WHERE ssn=? AND completato=0 AND id_es IS NOT NULL")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("id_vs"));
                    temp.setAnamnesi(rs.getString("anamnesi"));
                    temp.setCompleto(rs.getBoolean("completato"));
                    temp.setErog(rs.getDate("scadenza"));
                    temp.setId_ms(rs.getInt("id_ms"));
                    int ides = rs.getInt("id_es");
                    PreparedStatement ps2 = CON.prepareStatement("SELECT * FROM esame WHERE id_es=?");
                    ps2.setInt(1,ides);
                    ResultSet rs2 = ps2.executeQuery();
                    rs2.next();
                    temp.setEsame(rs2.getString("tipo"));
                    temp.setMs(rs.getString("medicospecialista.cognome")+rs.getString("medicospecialista.nome"));
                    rs2.close();
                    ps2.close();
                    esami.add(temp);
                }
                if(!esami.isEmpty())
                    Collections.sort(esami);
                rs.close();
                ps.close();
            }
            return esami;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public List<Ticket> getTickets(String ssn) throws DAOException {
        List<Ticket> tickets = new ArrayList<>();
        if(ssn == null)
            throw new DAOException("ssn is null");
        try(PreparedStatement ps = CON.prepareStatement("SELECT ticketb.* FROM visitamedicobase JOIN reportvisitab ON visitamedicobase.id_vb=reportvisitab.id_vb JOIN ticketb ON reportvisitab.ID_Report=ticketb.ID_Report WHERE ssn=? AND pagato=0")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    Ticket temp = new Ticket();
                    temp.setId_t(rs.getInt("id_tb"));
                    temp.setId_report(rs.getInt("id_report"));
                    temp.setErog(rs.getDate("data_erog"));
                    temp.setPagato(rs.getBoolean("pagato"));
                    temp.setValue(rs.getFloat("valore"));
                    temp.setTipo("base");
                    tickets.add(temp);
                }
                PreparedStatement ps2 = CON.prepareStatement("SELECT tickets.* FROM visitamedicospec JOIN reportvisitas ON visitamedicospec.id_vs=reportvisitas.id_vs JOIN tickets ON reportvisitas.ID_Report=tickets.ID_Report WHERE ssn=? AND pagato=0");
                ps2.setString(1, ssn);
                ResultSet rs2 = ps2.executeQuery();
                while(rs2.next()){
                    Ticket temp = new Ticket();
                    temp.setId_t(rs2.getInt("id_ts"));
                    temp.setId_report(rs2.getInt("id_report"));
                    temp.setErog(rs2.getDate("data_erog"));
                    temp.setPagato(rs2.getBoolean("pagato"));
                    temp.setValue(rs2.getFloat("valore"));
                    temp.setTipo("spec");
                    tickets.add(temp);
                }
                PreparedStatement ps3 = CON.prepareStatement("SELECT ticketssp.* FROM visitassp JOIN reportesamessp ON visitassp.id_vssp=reportesamessp.id_vssp JOIN ticketssp ON reportesamessp.ID_Report=ticketssp.ID_Report WHERE ssn=? AND pagato=0");
                ps3.setString(1, ssn);
                ResultSet rs3 = ps3.executeQuery();
                while(rs3.next()){
                    Ticket temp = new Ticket();
                    temp.setId_t(rs3.getInt("id_tssp"));
                    temp.setId_report(rs3.getInt("id_report"));
                    temp.setErog(rs3.getDate("data_erog"));
                    temp.setPagato(rs3.getBoolean("pagato"));
                    temp.setValue(rs3.getFloat("valore"));
                    temp.setTipo("ssp");
                    tickets.add(temp);
                }
                Collections.sort(tickets);
                rs2.close();
                rs3.close();
                rs.close();
                ps2.close();
                ps3.close();
                ps.close();
            }
            return tickets;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public Report getReportByPrimaryKey(Integer id_report, String type) throws DAOException {
        Report rep = new Report();
        try{
            switch(type){
                case "base":
                    PreparedStatement ps1 = CON.prepareStatement("SELECT * FROM reportvisitab WHERE id_report=?");
                    ps1.setInt(1, id_report);
                    ResultSet rs1 = ps1.executeQuery();
                    rs1.next();
                    rep.setId_rep(rs1.getInt("id_report"));
                    rep.setId_vis(rs1.getInt("id_vb"));
                    rep.setErog(rs1.getDate("data_erog"));
                    rep.setEsame("Visita medico base");
                    rep.setRisultato(rs1.getString("risultato"));
                    rs1.close();
                    ps1.close();
                    break;
                case "spec":
                    PreparedStatement ps2 = CON.prepareStatement("SELECT reportvisitas.*,esame.tipo FROM visitamedicospec LEFT JOIN esame ON esame.id_es=visitamedicospec.id_es JOIN reportvisitas ON visitamedicospec.id_vs=reportvisitas.id_vs WHERE id_report=?");
                    ps2.setInt(1, id_report);
                    ResultSet rs2 = ps2.executeQuery();
                    rs2.next();
                    rep.setId_rep(rs2.getInt("reportvisitas.id_report"));
                    rep.setId_vis(rs2.getInt("reportvisitas.id_vs"));
                    rep.setErog(rs2.getDate("reportvisitas.data_erog"));
                    if(rs2.getString("esame.tipo")==null)
                        rep.setEsame("Visita specialistica");
                    else
                        rep.setEsame(rs2.getString("esame.tipo"));
                    rep.setRisultato(rs2.getString("reportvisitas.risultato"));
                    rs2.close();
                    ps2.close();
                    break;
                case "ssp":
                    PreparedStatement ps3 = CON.prepareStatement("SELECT reportesamessp.*,esame.tipo FROM esame JOIN visitassp ON esame.id_es=visitassp.id_es JOIN reportesamessp ON visitassp.id_vssp=reportesamessp.id_vssp WHERE id_report=?");
                    ps3.setInt(1, id_report);
                    ResultSet rs3 = ps3.executeQuery();
                    rs3.next();
                    rep.setId_rep(rs3.getInt("reportesamessp.id_report"));
                    rep.setId_vis(rs3.getInt("reportesamessp.id_vssp"));
                    rep.setErog(rs3.getDate("reportesamessp.data_erog"));
                    rep.setEsame(rs3.getString("esame.tipo"));
                    rep.setRisultato(rs3.getString("reportesamessp.risultato"));
                    rs3.close();
                    ps3.close();
                    break;
            }
            return rep;
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public void updateTicket(int id, String type) throws DAOException {
        try{
            switch(type){
            case "base":
                PreparedStatement ps = CON.prepareStatement("UPDATE ticketb SET pagato=1 WHERE id_tb=?");
                ps.setInt(1,id);
                ps.execute();
                ps.close();
                break;
            case "spec":
                PreparedStatement ps2 = CON.prepareStatement("UPDATE tickets SET pagato=1 WHERE id_ts=?");
                ps2.setInt(1,id);
                ps2.execute();
                ps2.close();
                break;
            case "ssp":
                PreparedStatement ps3 = CON.prepareStatement("UPDATE ticketssp SET pagato=1 WHERE id_tssp=?");
                ps3.setInt(1,id);
                ps3.execute();
                ps3.close();
                break;
            }
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    @Override
    public String getSaltByEmail(String mail) throws DAOException {
        String salt = null;
        try(PreparedStatement ps = CON.prepareStatement("SELECT salt FROM loginutente WHERE email=?")){
            ps.setString(1, mail);
            try(ResultSet rs = ps.executeQuery()){
                rs.next();
                salt = rs.getString("salt");
                rs.close();
                ps.close();
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the salt for the passed mail", ex);
        }
    }

    @Override
    public void updateHash(String hash, String mail) throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("UPDATE loginutente SET hash=? WHERE email=?")){
            ps.setString(1, hash);
            ps.setString(2, mail);
            ps.execute();
            ps.close();
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update", ex);
        }
    }

    @Override
    public Paziente updatePhoto(String name, String ssn) throws DAOException {
        Paziente p = null;
        try(PreparedStatement ps = CON.prepareStatement("UPDATE paziente SET foto_path=? WHERE ssn=?")){
            ps.setString(1, name);
            ps.setString(2, ssn);
            ps.execute();
            ps.close();
        }catch(SQLException ex){
            throw new DAOException("Impossible to update", ex);
        }
        p = this.getByPrimaryKey(ssn);
        return p;
    }
    
}
