/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

import java.sql.Date;

/**
 *
 * @author simon
 */
public class Paziente {
    
    private String SSN, nome, cognome, email, foto, provincia;
    private MedicoBase mb;
    private char sex;
    private Date data;

    public String getSSN() {
        return SSN;
    }

    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public MedicoBase getMb() {
        return mb;
    }

    public void setMb(MedicoBase mb) {
        this.mb = mb;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return SSN+", "+nome+", "+cognome+", "+sex+", "+email+", "+foto+", "+provincia+", "+data+", medico: Dott. "
                + mb.getCognome()+" "+mb.getNome()+" ("+mb.getCitta()+","+mb.getProvincia()+")";
    }
  
    public String getDoc(){
        return "Dott. "+ mb.getCognome()+" "+mb.getNome();
    }
    
}
