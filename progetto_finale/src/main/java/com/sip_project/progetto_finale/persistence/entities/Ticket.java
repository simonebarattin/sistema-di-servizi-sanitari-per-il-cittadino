/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

import java.sql.Date;

/**
 *
 * @author simon
 */
public class Ticket implements Comparable<Ticket>{
    
    private int id_t, id_report;
    private float value;
    private boolean pagato;
    private Date erog;
    private String tipo;

    public int getId_t() {
        return id_t;
    }

    public void setId_t(int id_t) {
        this.id_t = id_t;
    }

    public int getId_report() {
        return id_report;
    }

    public void setId_report(int id_report) {
        this.id_report = id_report;
    }

    public boolean isPagato() {
        return pagato;
    }

    public void setPagato(boolean pagato) {
        this.pagato = pagato;
    }

    public Date getErog() {
        return erog;
    }

    public void setErog(Date erog) {
        this.erog = erog;
    }
    
    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public int compareTo(Ticket o) {
        if (getErog()== null || o.getErog()== null)
            return 0;
        return getErog().compareTo(o.getErog());
    }
    
}
