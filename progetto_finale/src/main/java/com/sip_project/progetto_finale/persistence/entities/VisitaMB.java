/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

import java.sql.Date;

/**
 *
 * @author simon
 */
public class VisitaMB implements Comparable<VisitaMB>{
    
    private int id_vb,id_mb;
    private Date scadenza;
    private String anamnesi, ssn, mb;
    private boolean completo;

    public String getMb() {
        return mb;
    }

    public void setMb(String mb) {
        this.mb = mb;
    }

    public int getId_vb() {
        return id_vb;
    }

    public void setId_vb(int id_vb) {
        this.id_vb = id_vb;
    }

    public int getId_mb() {
        return id_mb;
    }

    public void setId_mb(int id_mb) {
        this.id_mb = id_mb;
    }

    public Date getScadenza() {
        return scadenza;
    }

    public void setScadenza(Date scadenza) {
        this.scadenza = scadenza;
    }

    public String getAnamnesi() {
        return anamnesi;
    }

    public void setAnamnesi(String anamnesi) {
        this.anamnesi = anamnesi;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public boolean isCompleto() {
        return completo;
    }

    public void setCompleto(boolean completo) {
        this.completo = completo;
    }

    @Override
    public int compareTo(VisitaMB o) {
        if (getScadenza()== null || o.getScadenza()== null)
            return 0;
        return getScadenza().compareTo(o.getScadenza());

    }

    
    
}
