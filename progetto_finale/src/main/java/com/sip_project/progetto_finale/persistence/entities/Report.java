/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

import java.sql.Date;

/**
 *
 * @author simon
 */
public class Report {
    
    private int id_rep,id_vis;
    private Date erog;
    private String risultato, esame;

    public int getId_rep() {
        return id_rep;
    }

    public void setId_rep(int id_rep) {
        this.id_rep = id_rep;
    }

    public int getId_vis() {
        return id_vis;
    }

    public void setId_vis(int id_vis) {
        this.id_vis = id_vis;
    }

    public Date getErog() {
        return erog;
    }

    public void setErog(Date erog) {
        this.erog = erog;
    }

    public String getRisultato() {
        return risultato;
    }

    public void setRisultato(String risultato) {
        this.risultato = risultato;
    }

    public String getEsame() {
        return esame;
    }

    public void setEsame(String esame) {
        this.esame = esame;
    }    

    @Override
    public String toString() {
        return "Report{" + "id_rep=" + id_rep + ", id_vis=" + id_vis + ", erog=" + erog + ", risultato=" + risultato + ", esame=" + esame + '}';
    }
    
    
}
