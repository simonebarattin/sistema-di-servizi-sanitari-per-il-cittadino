/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;


import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.FarmaciDAO;
import com.sip_project.progetto_finale.persistence.entities.Farmaci;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author pietr
 */
public class JDBCFarmaciDAO extends JDBCDAO<Farmaci, Integer > implements FarmaciDAO {

    public JDBCFarmaciDAO(Connection con){
        super(con);
    }
    
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Farmaci getByPrimaryKey(Integer arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Farmaci> getAll() throws DAOException {
         try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM farmaco");){
            List<Farmaci> farm = new ArrayList<>();
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next())
                {
                    Farmaci temp= new Farmaci();
                    temp.setNome(rs.getString("nome"));
                    temp.setId_farm(rs.getInt("id_farm"));
                    //temp.setErog(rs.getString("erog"));
                    farm.add(temp);
                    }
                rs.close();
                ps.close();
                return farm;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the exam ", e);
        }
        }

    @Override
    public Farmaci getByType(String arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public void prescFarm( String arg1, int arg2, String arg3, int arg4) throws DAOException {
       try(PreparedStatement ps = CON.prepareStatement("insert into prescrizionefarmaco(ssn,id_farm,data_erog,ritirato)"+" values(?,?,?,?)");){
            ps.setString(1,arg1 );
            ps.setInt(2,arg2 );
            ps.setString(3,arg3 );
            ps.setInt(4,arg4 );
            ps.execute();
            ps.close();
            
        }catch(SQLException e){
            throw new DAOException("impossibile update");
        }
    }

    @Override
    public int getFromId(int arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PrescrizioneFarmaco> getPresc(String ssn, int id_mb) throws DAOException {
        List <PrescrizioneFarmaco> farm= new ArrayList();
        
        try(PreparedStatement ps = CON.prepareStatement("select * from prescrizionefarmaco join paziente on prescrizionefarmaco.ssn=paziente.ssn join farmaco on prescrizionefarmaco.id_farm=farmaco.id_farm where prescrizionefarmaco.SSN=? and id_mb=?");){
            ps.setString(1, ssn);
            ps.setInt(2,id_mb);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                PrescrizioneFarmaco temp = new PrescrizioneFarmaco();
                temp.setErog(rs.getDate("data_erog"));
                temp.setId_farm(rs.getInt("ID_FARM"));
                //temp.setId_mb(rs.getInt("id_mb"));
                temp.setId_ric(rs.getInt("id_ric"));
                //temp.setMedico(rs.getString(""));
                temp.setNome(rs.getString("farmaco.nome"));
                //temp.setPaziente(ssn);
                temp.setRitirato(rs.getBoolean("ritirato"));
               // temp.setSsn();
                farm.add(temp);
                System.out.println("\n\n\n\n "+farm.size());
                
                }
                rs.close();
                ps.close();
            }
    
        }       catch (SQLException ex) {
            Logger.getLogger(JDBCFarmaciDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        return farm;
}
}

