/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.VisitaMSDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simon
 */
public class JDBCVisitaMSDAO extends JDBCDAO<VisitaMS, Integer> implements VisitaMSDAO{
    
    public JDBCVisitaMSDAO(Connection con){
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VisitaMS getByPrimaryKey(Integer id_vs) throws DAOException {
        VisitaMS visita = new VisitaMS();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN paziente ON visitamedicospec.ssn=paziente.ssn JOIN medicobase ON visitamedicospec.id_mb=medicobase.id_mb LEFT JOIN esame ON visitamedicospec.id_es=esame.id_es WHERE id_vs=?")){
            ps.setInt(1, id_vs);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    visita.setId_vs(id_vs);
                    visita.setAnamnesi(rs.getString("visitamedicospec.anamnesi"));
                    visita.setCompleto(rs.getBoolean("visitamedicospec.completato"));
                    visita.setErog(rs.getDate("visitamedicospec.scadenza"));
                    visita.setId_ms(rs.getInt("visitamedicospec.id_ms"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    paz.setEmail(rs.getString("paziente.email"));
                    visita.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    visita.setMb(mb);
                    visita.setEsame(rs.getString("esame.tipo"));
                }
                rs.close();
                ps.close();
                return visita;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the visit for the passed ssn", e);
        }
    }

    @Override
    public List<VisitaMS> getAll() throws DAOException {
        List<VisitaMS> visite = new ArrayList<>();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN paziente ON visitamedicospec.ssn=paziente.ssn JOIN medicobase ON visitamedicospec.id_mb=medicobase.id_mb JOIN esame ON visitamedicospec.id_es=esame.id_es WHERE completato=0")){
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("visitamedicospec.id_vs"));
                    temp.setAnamnesi(rs.getString("visitamedicospec.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicospec.completato"));
                    temp.setErog(rs.getDate("visitamedicospec.data_erog"));
                    temp.setId_ms(rs.getInt("visitamedicospec.id_ms"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    temp.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    temp.setMb(mb);
                    temp.setEsame(rs.getString("esame.tipo"));
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the visit for the passed ssn", e);
        }
    }

    @Override
    public List<VisitaMS> getBySSN(String ssn) throws DAOException {
        List<VisitaMS> visite = new ArrayList<>();
        if(ssn==null){
            throw new DAOException("ssn is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN paziente ON visitamedicospec.ssn=paziente.ssn JOIN medicobase ON visitamedicospec.id_mb=medicobase.id_mb LEFT JOIN esame ON visitamedicospec.id_es=esame.id_es WHERE paziente.ssn=?")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("visitamedicospec.id_vs"));
                    temp.setAnamnesi(rs.getString("visitamedicospec.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicospec.completato"));
                    temp.setErog(rs.getDate("visitamedicospec.scadenza"));
                    temp.setId_ms(rs.getInt("visitamedicospec.id_ms"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    temp.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    temp.setMb(mb);
                    if(rs.getString("esame.tipo") == null)
                        temp.setEsame("Visita specialistica");
                    else
                        temp.setEsame(rs.getString("esame.tipo"));
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the visit for the passed ssn", e);
        }
    }

    @Override
    public void updateAnamnesi(String anamnesi, int id_vs) throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("UPDATE visitamedicospec SET anamnesi=?, completato=1 WHERE id_vs=?")){
            ps.setString(1,anamnesi);
            ps.setInt(2, id_vs);
            ps.executeUpdate();
            ps.close();
        }catch(SQLException e){
            System.err.print(e);
        }
    }

    @Override
    public List<VisitaMS> getAll(int id_ms) {
        List<VisitaMS> visite = new ArrayList<>();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN paziente ON visitamedicospec.ssn=paziente.ssn JOIN medicobase ON visitamedicospec.id_mb=medicobase.id_mb LEFT JOIN esame ON visitamedicospec.id_es=esame.id_es WHERE completato=0 AND id_ms=?")){
            ps.setInt(1, id_ms);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("visitamedicospec.id_vs"));
                    temp.setAnamnesi(rs.getString("visitamedicospec.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicospec.completato"));
                    temp.setErog(rs.getDate("visitamedicospec.scadenza"));
                    temp.setId_ms(rs.getInt("visitamedicospec.id_ms"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    temp.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    temp.setMb(mb);
                    String esame = rs.getString("esame.tipo");
                    if(esame == null)
                        temp.setEsame("Visita specialistica");
                    else 
                        temp.setEsame(esame);
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            System.err.print(e);
        }
        return null;
    }

    @Override
    public List<VisitaMS> getVisiteCompletate(int arg0) throws DAOException {
        List<VisitaMS> visite = new ArrayList<>();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN paziente ON visitamedicospec.ssn=paziente.ssn WHERE id_ms=? AND completato=1 AND id_es IS NULL AND erogato=0")){
            ps.setInt(1, arg0);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("visitamedicospec.id_vs"));
                    temp.setAnamnesi(rs.getString("visitamedicospec.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicospec.completato"));
                    temp.setErog(rs.getDate("visitamedicospec.scadenza"));
                    temp.setId_ms(rs.getInt("visitamedicospec.id_ms"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    paz.setEmail(rs.getString("paziente.email"));
                    temp.setPaz(paz);
                    temp.setEsame(null);
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            System.err.print(e);
        }
        return null;
    }

    @Override
    public List<VisitaMS> getEsamiCompletati(int arg0) throws DAOException {
        List<VisitaMS> visite = new ArrayList<>();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicospec JOIN paziente ON visitamedicospec.ssn=paziente.ssn JOIN esame ON visitamedicospec.id_es=esame.id_es WHERE id_ms=? AND completato=1 AND esame.id_es IS NOT NULL AND erogato=0")){
            ps.setInt(1, arg0);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaMS temp = new VisitaMS();
                    temp.setId_vs(rs.getInt("visitamedicospec.id_vs"));
                    temp.setAnamnesi(rs.getString("visitamedicospec.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicospec.completato"));
                    temp.setErog(rs.getDate("visitamedicospec.scadenza"));
                    temp.setId_ms(rs.getInt("visitamedicospec.id_ms"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    paz.setEmail(rs.getString("paziente.email"));
                    temp.setPaz(paz);
                    temp.setEsame(rs.getString("esame.tipo"));
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            System.err.print(e);
        }
        return null;
    }

    @Override
    public void updateReport(String result, int id_vs) throws DAOException {
        int id_rep = 0;
        try(PreparedStatement ps = CON.prepareStatement("INSERT INTO reportvisitas(id_vs,data_erog,risultato) VALUES (?,now(),?)")){
            ps.setInt(1,id_vs);
            ps.setString(2, result);
            ps.executeUpdate();
            ps.close();
        }catch(SQLException e){
            System.err.print(e);
        }
        try(PreparedStatement ps2 = CON.prepareStatement("SELECT * FROM reportvisitas WHERE id_vs=?")){
            ps2.setInt(1, id_vs);
            try(ResultSet rs2 = ps2.executeQuery()){
                rs2.next();
                id_rep = rs2.getInt("id_report");
                rs2.close();
                ps2.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCVisitaMSDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         try(PreparedStatement ps3 = CON.prepareStatement("INSERT INTO tickets(id_report,pagato,data_erog,valore) VALUES (?,0,now(),50)")){
            ps3.setInt(1,id_rep);
            ps3.executeUpdate();
            ps3.close();
        }catch(SQLException e){
            System.err.print(e);
        }
        try(PreparedStatement ps4 = CON.prepareStatement("UPDATE visitamedicospec SET erogato=1 WHERE id_vs=?")){
            ps4.setInt(1,id_vs);
            ps4.executeUpdate();
            ps4.close();
        }catch(SQLException e){
            System.err.print(e);
        }
    }
    
}
