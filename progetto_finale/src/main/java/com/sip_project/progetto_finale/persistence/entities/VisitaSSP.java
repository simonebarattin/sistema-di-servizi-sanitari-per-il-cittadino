/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

import java.sql.Date;

/**
 *
 * @author simon
 */
public class VisitaSSP implements Comparable<VisitaSSP>{
    
    private int id_vssp,id_ssp;
    private Date scadenza;
    private String esame;
    private boolean completo;
    private Paziente paz;

    public Paziente getPaz() {
        return paz;
    }

    public void setPaz(Paziente paz) {
        this.paz = paz;
    }
    
    

    public int getId_vssp() {
        return id_vssp;
    }

    public void setId_vssp(int id_vssp) {
        this.id_vssp = id_vssp;
    }

    public int getId_ssp() {
        return id_ssp;
    }

    public void setId_ssp(int id_ssp) {
        this.id_ssp = id_ssp;
    }

    public Date getScadenza() {
        return scadenza;
    }

    public void setScadenza(Date scadenza) {
        this.scadenza = scadenza;
    }

    public String getEsame() {
        return esame;
    }

    public void setEsame(String esame) {
        this.esame = esame;
    }

    public boolean isCompleto() {
        return completo;
    }

    public void setCompleto(boolean completo) {
        this.completo = completo;
    }

    @Override
    public int compareTo(VisitaSSP o) {
        if (getScadenza()== null || o.getScadenza()== null)
            return 0;
        return getScadenza().compareTo(o.getScadenza());

    }
}
