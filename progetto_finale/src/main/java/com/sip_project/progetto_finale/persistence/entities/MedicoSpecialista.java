/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

/**
 * Entità che rappresenta il Medico Specialista
 * 
 * @author simon
 */
public class MedicoSpecialista {
    
    private int id;
    private String nome,cognome,provincia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @Override
    public String toString() {
        return "MedicoSpecialista{" + "id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", provincia=" + provincia + '}';
    }
    
    
}
