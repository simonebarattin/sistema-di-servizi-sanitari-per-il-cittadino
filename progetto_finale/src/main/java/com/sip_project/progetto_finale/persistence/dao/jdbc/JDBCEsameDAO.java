/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.EsameDAO;
import com.sip_project.progetto_finale.persistence.entities.Esame;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simon
 */
public class JDBCEsameDAO extends JDBCDAO<Esame, Integer> implements EsameDAO{

    public JDBCEsameDAO(Connection con){
        super(con);
    }
    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Esame getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM esame WHERE id_es=?");){
            ps.setInt(1, primaryKey);
            try(ResultSet rs = ps.executeQuery();){
                rs.next();
                Esame temp = new Esame();
                temp.setId(rs.getInt("id_es"));
                temp.setTipo(rs.getString("tipo"));
                temp.setErog(rs.getString("erog"));
                rs.close();
                ps.close();
                return temp;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the exam for the passed primary key", e);
        }
    }

    @Override
    public List<Esame> getAllSpec() throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM esame WHERE erog=?");){
            ps.setString(1,"s");
            List<Esame> esami = new ArrayList<Esame>();
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next())
                {
                    Esame temp= new Esame();
                    temp.setId(rs.getInt("id_es"));
                    temp.setTipo(rs.getString("tipo"));
                    //temp.setErog(rs.getString("erog"));
                    esami.add(temp);
                    }
                rs.close();
                ps.close();
                return esami;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the exam ", e);
        }
    }

    @Override
    public Esame getByType(String arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void esameSpec( String arg1, int arg2, int arg3, String arg4, int arg5, int arg6,int arg7) throws DAOException {
       try(PreparedStatement ps = CON.prepareStatement("insert into visitamedicospec(scadenza,completato,erogato,ssn,id_ms,id_mb,id_es)"+" values(?,?,?,?,?,?,?)");){
            ps.setString(1,arg1 );
            ps.setInt(2,arg2 );
            ps.setInt(3,arg3 );
            ps.setString(4,arg4 );
            ps.setInt(5,arg5 );
            ps.setInt(6,arg6 );
            ps.setInt(7,arg7 );
            ps.execute();
            ps.close();
            
        }catch(SQLException e){
            throw new DAOException("impossibile update");
        }
    }

    @Override
    public void esameSsn(String data, int completato, int erogato, String SSN, int id_ssp, int id_es) throws DAOException {
        
        try(PreparedStatement ps = CON.prepareStatement("insert into visitassp(scadenza,ssn,completato,erogato,id_ssp,id_es)"+" values(?,?,?,?,?,?)");){
            ps.setString(1,data );
            ps.setString(2,SSN);
            ps.setInt(3, completato );
            ps.setInt(4, erogato );
            ps.setInt(5,id_ssp );
            ps.setInt(6,id_es );
            ps.executeUpdate();
            ps.close();
            
        }catch(SQLException e){
            throw new DAOException("impossibile update");
        }
    }

    @Override
    public int getFromProvicia(String provincia) throws DAOException {
        
           try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM ssp WHERE sp=?");){
            ps.setString(1, provincia);
            try(ResultSet rs = ps.executeQuery();){
                rs.next();
                int idssp=rs.getInt("id_ssp");
                return idssp;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the exam for the passed primary key", e);
        }
    }

    @Override
    public List<Esame> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Esame> getAllSsp() throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM esame WHERE erog=?");){
            ps.setString(1,"p");
            List<Esame> esami = new ArrayList<Esame>();
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next())
                {
                    Esame temp= new Esame();
                    temp.setId(rs.getInt("id_es"));
                    temp.setTipo(rs.getString("tipo"));
                    //temp.setErog(rs.getString("erog"));
                    esami.add(temp);
                    }
                rs.close();
                ps.close();
                return esami;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the exam ", e);
        }
    }
    
}
