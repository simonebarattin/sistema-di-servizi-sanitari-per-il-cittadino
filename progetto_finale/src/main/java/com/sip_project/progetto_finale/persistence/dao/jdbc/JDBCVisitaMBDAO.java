/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.VisitaMBDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.VisitaMB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pietr
 */


//aggiungi medico di base e paziente a VisitaMB.java


public class JDBCVisitaMBDAO extends JDBCDAO<VisitaMB, Integer> implements VisitaMBDAO {
        
    public JDBCVisitaMBDAO(Connection con){
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VisitaMB getByPrimaryKey(Integer id_vs) throws DAOException {
        VisitaMB visita = new VisitaMB();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicobase JOIN paziente ON visitamedicobase.ssn=paziente.ssn JOIN medicobase ON visitamedicobase.id_mb=medicobase.id_mb where ID_VB=?")){
            ps.setInt(1, id_vs);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    visita.setId_vb(id_vs);
                    visita.setAnamnesi(rs.getString("visitamedicobase.anamnesi"));
                    visita.setCompleto(rs.getBoolean("visitamedicobase.completato"));
                    visita.setId_mb(rs.getInt("visitamedicobase.id_ms"));
                    visita.setScadenza(rs.getDate("visitamedicobase.scadenza"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    //visita.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    //visita.setMb(mb);
                    //visita.setEsame(rs.getString("esame.tipo"));
                }
                rs.close();
                ps.close();
                return visita;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the visit for the passed ssn", e);
        }
    }

    @Override
    public List<VisitaMB> getAll() throws DAOException {
        List<VisitaMB> visite = new ArrayList<>();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicobase JOIN paziente ON visitamedicobase.ssn=paziente.ssn JOIN medicobase ON visitamedicobase.id_mb=medicobase.id_mb WHERE completato=0")){
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    VisitaMB temp = new VisitaMB();
                    temp.setId_vb(rs.getInt("visitamedicobase.id_vs"));
                    temp.setAnamnesi(rs.getString("visitamedicobase.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicobase.completato"));
                    temp.setScadenza(rs.getDate("visitamedicobase.scadenza"));
                    temp.setId_mb(rs.getInt("visitamedicobase.id_ms"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    //temp.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    //temp.setMb(mb);
                    //temp.setEsame(rs.getString("esame.tipo"));
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the visit for the passed ssn", e);
        }
    }

    @Override
    public List<VisitaMB> getBySSN(String ssn) throws DAOException {
        List<VisitaMB> visite = new ArrayList<>();
        if(ssn==null){
            throw new DAOException("ssn is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicobase JOIN paziente ON visitamedicobase.ssn=paziente.ssn JOIN medicobase ON visitamedicobase.id_mb=medicobase.id_mb WHERE paziente.ssn=? AND completato=1")){
            ps.setString(1, ssn);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    VisitaMB temp = new VisitaMB();
                    temp.setId_vb(rs.getInt("visitamedicobase.id_vb"));
                    temp.setAnamnesi(rs.getString("visitamedicobase.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicobase.completato"));
                    temp.setScadenza(rs.getDate("visitamedicobase.scadenza"));
                    temp.setId_mb(rs.getInt("visitamedicobase.id_mb"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    //temp.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    //temp.setMb(mb);
                    //temp.setEsame(rs.getString("esame.tipo"));
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the visit for the passed ssn", e);
        }
    }

    @Override
    public void updateAnamnesi(String anamnesi, int id_vb) throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("UPDATE visitamedicobase SET anamnesi=?, completato=1 WHERE id_vb=?")){
            ps.setString(1,anamnesi);
            ps.setInt(2, id_vb);
            ps.executeUpdate();
            ps.close();
        }catch(SQLException e){
            System.err.print(e);
        }
    }

    @Override
    public List<VisitaMB> getAll(int id_mb) {
        List<VisitaMB> visite = new ArrayList<>();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitamedicobase JOIN paziente ON visitamedicobase.ssn=paziente.ssn JOIN medicobase ON visitamedicobase.id_mb=medicobase.id_mb  WHERE completato=0 AND id_mb=?")){
            ps.setInt(1, id_mb);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    VisitaMB temp = new VisitaMB();
                    temp.setId_vb(rs.getInt("visitamedicobase.id_vb"));
                    temp.setAnamnesi(rs.getString("visitamedicobase.anamnesi"));
                    temp.setCompleto(rs.getBoolean("visitamedicobase.completato"));
                    temp.setScadenza(rs.getDate("visitamedicobase.scadenza"));
                    temp.setId_mb(rs.getInt("visitamedicospec.id_mb"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    //temp.setPaz(paz);
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    
                    visite.add(temp);
                }
                rs.close();
                ps.close();
                return visite;
            }
        }catch(SQLException e){
            System.err.print(e);
        }
        return null;
    }
    
}
