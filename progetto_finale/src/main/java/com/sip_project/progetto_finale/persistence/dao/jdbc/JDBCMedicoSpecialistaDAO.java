/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.MedicoSpecialistaDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.MedicoSpecialista;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.servlets.Encrypt;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simon
 */
public class JDBCMedicoSpecialistaDAO extends JDBCDAO<MedicoSpecialista, Integer> implements MedicoSpecialistaDAO{
    
    public JDBCMedicoSpecialistaDAO(Connection con){
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MedicoSpecialista getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM medicospecialista WHERE id_ms=?");){
            ps.setInt(1, primaryKey);
            try(ResultSet rs = ps.executeQuery();){
                rs.next();
                MedicoSpecialista temp = new MedicoSpecialista();
                temp.setId(rs.getInt("id_ms"));
                temp.setNome(rs.getString("nome"));
                temp.setCognome(rs.getString("cognome"));
                temp.setProvincia(rs.getString("sp"));
                rs.close();
                ps.close();
                return temp;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the user for the passed primary key", e);
        }
    }

    @Override
    public List<MedicoSpecialista> getAll() throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM medicospecialista");){
            List<MedicoSpecialista> ms = new ArrayList<MedicoSpecialista>();
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next())
                {
                    MedicoSpecialista temp= new MedicoSpecialista();
                    temp.setId(rs.getInt("id_ms"));
                    temp.setNome(rs.getString("nome"));
                    temp.setCognome(rs.getString("cognome"));
                    temp.setProvincia(rs.getString("sp"));
                    ms.add(temp);
                    }
                rs.close();
                ps.close();
                return ms;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the exam ", e);
        }}

    @Override
    public MedicoSpecialista getByEmailAndPassword(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) {
            throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));
        }
        
        try (PreparedStatement ps = CON.prepareStatement("SELECT * FROM loginspecialista WHERE email=?")) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            rs.next();
            int id_ms = rs.getInt("id_ms");
            String salt = rs.getString("salt");
            String hash = rs.getString("hash");
            Encrypt enc = new Encrypt();
            String res = "";
            try {
                res = enc.getHash(password, salt);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(JDBCMedicoSpecialistaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.close();
            rs.close();
            if(res.equals(hash)){
                PreparedStatement log = CON.prepareStatement("SELECT medicospecialista.id_ms,medicospecialista.nome,cognome,medicospecialista.sp FROM loginspecialista JOIN medicospecialista ON loginspecialista.id_ms = medicospecialista.id_ms "
                        + "JOIN provincia ON medicospecialista.sp = provincia.sp WHERE medicospecialista.id_ms=?");
                log.setInt(1, id_ms);
                ResultSet ris = log.executeQuery();
                ris.next();
                MedicoSpecialista ms = new MedicoSpecialista();
                ms.setId(ris.getInt("id_ms"));
                ms.setNome(ris.getString("nome"));
                ms.setCognome(ris.getString("cognome"));
                ms.setProvincia(ris.getString("sp"));
                log.close();
                ris.close();
                return ms;
            }
            return null;
        } catch (SQLException ex) {
            return null;
        } 
    }
    
    public List<Paziente> getPatients() throws SQLException{
        List<Paziente> patients = new ArrayList<>();
        PreparedStatement ps = CON.prepareStatement("SELECT * FROM paziente");
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Paziente temp = new Paziente();
            temp.setSSN(rs.getString("ssn"));
            temp.setNome(rs.getString("nome"));
            temp.setCognome(rs.getString("cognome"));
            temp.setEmail(rs.getString("email"));
            temp.setSex(rs.getString("sesso").charAt(0));
            temp.setData(rs.getDate("datanascita"));
            temp.setFoto(rs.getString("foto_path"));
            temp.setProvincia(rs.getString("sp"));
            int id_mb = rs.getInt("id_mb");
            MedicoBase mb = new MedicoBase();
            PreparedStatement ps2 = CON.prepareStatement("SELECT * FROM medicobase WHERE id_mb=?");
            ps2.setInt(1,id_mb);
            ResultSet rs2 = ps2.executeQuery();
            rs2.next();
            mb.setId(rs2.getInt("id_mb"));
            mb.setNome(rs2.getString("nome"));
            mb.setCognome(rs2.getString("cognome"));
            mb.setProvincia(rs2.getString("sp"));
            mb.setCitta(rs2.getString("citta"));
            temp.setMb(mb);
            patients.add(temp);
        }
        rs.close();
        ps.close();
        return patients;
    }

    @Override
    public String getSaltByEmail(String mail) throws DAOException {
        String salt = null;
        try(PreparedStatement ps = CON.prepareStatement("SELECT salt FROM loginspecialista WHERE email=?")){
            ps.setString(1, mail);
            try(ResultSet rs = ps.executeQuery()){
                rs.next();
                salt = rs.getString("salt");
                rs.close();
                ps.close();
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the salt for the passed mail", ex);
        }
    }

    @Override
    public void updateHash(String hash, String mail) throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("UPDATE loginspecialista SET hash=? WHERE email=?")){
            ps.setString(1, hash);
            ps.setString(2, mail);
            ps.execute();
            ps.close();
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update", ex);
        }
    }
}
