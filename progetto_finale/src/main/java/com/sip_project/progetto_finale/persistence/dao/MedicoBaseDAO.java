/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.Report;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author simon
 */
public interface MedicoBaseDAO extends DAO<MedicoBase, Integer> {
    
    public MedicoBase getByEmailAndPassword(String email, String password) throws DAOException;
    
    public List<Paziente> getPatients(int id_mb) throws SQLException;
    
    public MedicoBase update(MedicoBase user) throws DAOException;
    
    public List<Report> getReportS(int id_mb,String ssn,List<VisitaMS> vms )throws SQLException;
    
    public List<Report> getReportP(int id_mb,String ssn)throws SQLException;
    
    public String getSaltByEmail(String mail) throws DAOException;

    public void updateHash(String hash, String mail) throws DAOException;
}

