/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import com.sip_project.progetto_finale.persistence.entities.Report;
import com.sip_project.progetto_finale.persistence.entities.Ticket;
import com.sip_project.progetto_finale.persistence.entities.VisitaMB;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import com.sip_project.progetto_finale.persistence.entities.VisitaSSP;
import java.util.List;

/**
 *
 * @author simon
 */
public interface PazienteDAO extends DAO<Paziente, String>{
    
    public Paziente getByEmailAndPassword(String email, String password) throws DAOException;
    
    public Paziente update(Paziente user) throws DAOException;
    
    public Paziente updateMB(String ssn, int id_mb) throws DAOException;
    
    public List<MedicoBase> getMedici(String provincia) throws DAOException;
    
    public List<PrescrizioneFarmaco> getFarmaci(String ssn) throws DAOException;
    
    public List<VisitaMB> getVisiteBComplete(String ssn) throws DAOException;  
    
    public List<VisitaMB> getVisiteBNonComplete(String ssn) throws DAOException;  
    
    public List<VisitaSSP> getEsamiSSPComplete(String ssn) throws DAOException;  
    
    public List<VisitaSSP> getEsamiSSPNonComplete(String ssn) throws DAOException; 
    
    public List<VisitaMS> getVisiteSComplete(String ssn) throws DAOException;  
    
    public List<VisitaMS> getVisiteSNonComplete(String ssn) throws DAOException; 
    
    public List<VisitaMS> getEsamiSCompleti(String ssn) throws DAOException;
    
    public List<VisitaMS> getEsamiSNCompleti(String ssn) throws DAOException;

    public List<Ticket> getTickets(String ssn) throws DAOException;

    public Report getReportByPrimaryKey(Integer reportId, String type) throws DAOException;

    public void updateTicket(int id, String type) throws DAOException;

    public String getSaltByEmail(String mail) throws DAOException;

    public void updateHash(String hash, String mail) throws DAOException;

    public Paziente updatePhoto(String name, String ssn) throws DAOException;
}
