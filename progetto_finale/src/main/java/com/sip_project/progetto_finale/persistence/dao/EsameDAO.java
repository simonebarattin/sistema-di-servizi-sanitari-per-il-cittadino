/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.Esame;
import java.util.List;

/**
 *
 * @author simon
 */
public interface EsameDAO extends DAO<Esame, Integer>{
        

    public Esame getByType(String esame) throws DAOException;
    public List <Esame> getAllSpec() throws DAOException;
    public List <Esame> getAllSsp() throws DAOException;
    public void esameSpec( String data, int completato, int erogato, String SSN, int id_ms, int id_mb, int id_es ) throws DAOException;
    public void esameSsn(String data, int completato, int erogato, String SSN, int id_ssp, int id_es) throws DAOException;
    public int getFromProvicia(String provincia)throws DAOException;
    
}
