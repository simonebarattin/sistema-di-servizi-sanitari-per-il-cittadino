/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import com.sip_project.progetto_finale.persistence.entities.SSP;
import com.sip_project.progetto_finale.persistence.entities.VisitaSSP;
import java.util.List;

/**
 *
 * @author simon
 */
public interface SSPDAO extends DAO<SSP, Integer>{
    
    public List<PrescrizioneFarmaco> getPrescrizioni(String sp) throws DAOException;

    public List<VisitaSSP> getEsamiCompleti(int id_ssp) throws DAOException;

    public VisitaSSP getVisitByPrimaryKey(int id_vs) throws DAOException;

    public void updateReport(String result, int id_vssp) throws DAOException;
    
    public SSP getByEmailAndPassword(String email, String password) throws DAOException;    

    public String getSaltByEmail(String mail)throws DAOException;

    public void updateHash(String hash, String mail) throws DAOException;
}
