/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

/**
 *
 * @author pietr
 */
public class Farmaci {
    private String nome;
    private int id_farm;

    public String getNome() {
        return nome;
    }

    public int getId_farm() {
        return id_farm;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setId_farm(int id_farm) {
        this.id_farm = id_farm;
    }
    
    
}
