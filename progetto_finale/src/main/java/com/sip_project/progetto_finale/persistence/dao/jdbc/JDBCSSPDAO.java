/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.SSPDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.PrescrizioneFarmaco;
import com.sip_project.progetto_finale.persistence.entities.SSP;
import com.sip_project.progetto_finale.persistence.entities.VisitaSSP;
import com.sip_project.progetto_finale.servlets.Encrypt;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simon
 */
public class JDBCSSPDAO extends JDBCDAO<SSP, Integer> implements SSPDAO{
    
    public JDBCSSPDAO(Connection con){
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SSP> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public SSP getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM ssp WHERE id_ssp=?");){
            ps.setInt(1, primaryKey);
            try(ResultSet rs = ps.executeQuery();){
                rs.next();
                SSP temp = new SSP();
                temp.setId_ssp(primaryKey);
                temp.setProvincia(rs.getString("sp"));
                return temp;
            }            
        }catch(SQLException e){
            throw new DAOException("Impossible to get the user for the passed primary key", e);
        }
    }


    @Override
    public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> arg0) throws DAOFactoryException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PrescrizioneFarmaco> getPrescrizioni(String sp) throws DAOException {
        List<PrescrizioneFarmaco> presc = new ArrayList<>();
        if(sp==null){
            throw new DAOException("sp is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM prescrizionefarmaco JOIN paziente ON prescrizionefarmaco.ssn=paziente.ssn JOIN medicobase ON paziente.id_mb=medicobase.id_mb JOIN farmaco ON prescrizionefarmaco.id_farm=farmaco.id_farm WHERE paziente.sp=?")){
            ps.setString(1, sp);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                PrescrizioneFarmaco temp = new PrescrizioneFarmaco();
                temp.setId_ric(rs.getInt("prescrizionefarmaco.id_ric"));
                temp.setId_farm(rs.getInt("prescrizionefarmaco.id_farm"));
                temp.setNome(rs.getString("farmaco.nome"));
                temp.setSsn(rs.getString("prescrizionefarmaco.ssn"));
                temp.setRitirato(rs.getBoolean("prescrizionefarmaco.ritirato"));
                temp.setErog(rs.getDate("prescrizionefarmaco.data_erog"));
                temp.setId_mb(rs.getInt("paziente.id_mb"));
                temp.setPaziente(rs.getString("paziente.cognome") +" "+ rs.getString("paziente.nome"));
                temp.setMedico("Dott. "+ rs.getString("medicobase.cognome")+ " "+ rs.getString("medicobase.nome") );
                presc.add(temp);
            }
            return presc;
        }catch(SQLException e){
            System.err.print(e);
        }
        return null;
    }

    @Override
    public List<VisitaSSP> getEsamiCompleti(int arg0) throws DAOException {
        List<VisitaSSP> esami = new ArrayList<>();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitassp JOIN paziente ON visitassp.ssn=paziente.ssn JOIN esame ON visitassp.id_es=esame.id_es WHERE id_ssp=1 AND completato=1 AND esame.id_es IS NOT NULL AND erogato=0")){
            //ps.setInt(1, arg0);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    VisitaSSP temp = new VisitaSSP();
                    temp.setId_vssp(rs.getInt("visitassp.id_vssp"));
                    temp.setId_ssp(rs.getInt("visitassp.id_ssp"));
                    temp.setScadenza(rs.getDate("visitassp.scadenza"));
                    temp.setCompleto(rs.getBoolean("visitassp.completato"));
                    Paziente paz = new Paziente();
                    paz.setSSN(rs.getString("paziente.ssn"));
                    paz.setNome(rs.getString("paziente.nome"));
                    paz.setCognome(rs.getString("paziente.cognome"));
                    paz.setEmail(rs.getString("paziente.email"));
                    temp.setPaz(paz);
                    temp.setEsame(rs.getString("esame.tipo"));
                    esami.add(temp);
                }
                rs.close();
                ps.close();
                return esami;
            }
        }catch(SQLException e){
            System.err.print(e);
        }
        return null;
    }

    @Override
    public VisitaSSP getVisitByPrimaryKey(int id_vssp) throws DAOException {
        VisitaSSP visita = new VisitaSSP();
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM visitassp JOIN paziente ON visitassp.ssn=paziente.ssn JOIN medicobase ON paziente.id_mb=medicobase.id_mb JOIN esame ON visitassp.id_es=esame.id_es WHERE id_vssp=?")){
            ps.setInt(1, id_vssp);
            try(ResultSet rs = ps.executeQuery();){
                while(rs.next()){
                    visita.setId_vssp(rs.getInt("visitassp.id_vssp"));
                    visita.setId_ssp(rs.getInt("visitassp.id_ssp"));
                    visita.setCompleto(rs.getBoolean("visitassp.completato"));
                    visita.setEsame(rs.getString("esame.tipo"));
                    Paziente temp = new Paziente();
                    temp.setSSN(rs.getString("paziente.ssn"));
                    temp.setNome(rs.getString("paziente.nome"));
                    temp.setCognome(rs.getString("paziente.cognome"));
                    temp.setEmail(rs.getString("paziente.email"));
                    temp.setSex(rs.getString("paziente.sesso").charAt(0));
                    temp.setData(rs.getDate("paziente.datanascita"));
                    temp.setFoto(rs.getString("paziente.foto_path"));
                    temp.setProvincia(rs.getString("paziente.sp"));
                    MedicoBase mb = new MedicoBase();
                    mb.setId(rs.getInt("medicobase.id_mb"));
                    mb.setNome(rs.getString("medicobase.nome"));
                    mb.setCognome(rs.getString("medicobase.cognome"));
                    mb.setProvincia(rs.getString("medicobase.sp"));
                    mb.setCitta(rs.getString("medicobase.citta"));
                    temp.setMb(mb);
                    visita.setPaz(temp);
                }
                rs.close();
                ps.close();
                return visita;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the visit for the passed ssn", e);
        }
    }

    @Override
    public void updateReport(String result, int id_vssp) throws DAOException {
        int id_rep = 0;
        try(PreparedStatement ps = CON.prepareStatement("INSERT INTO reportesamessp(id_vssp,data_erog,risultato) VALUES (?,now(),?)")){
            ps.setInt(1,id_vssp);
            ps.setString(2, result);
            ps.executeUpdate();
            ps.close();
        }catch(SQLException e){
            System.err.print(e);
        }
        try(PreparedStatement ps2 = CON.prepareStatement("SELECT * FROM reportesamessp WHERE id_vssp=?")){
            ps2.setInt(1, id_vssp);
            try(ResultSet rs2 = ps2.executeQuery()){
                rs2.next();
                id_rep = rs2.getInt("id_report");
                rs2.close();
                ps2.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCVisitaMSDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         try(PreparedStatement ps3 = CON.prepareStatement("INSERT INTO ticketssp(id_report,pagato,data_erog,valore) VALUES (?,0,now(),11)")){
            ps3.setInt(1,id_rep);
            ps3.executeUpdate();
            ps3.close();
        }catch(SQLException e){
            System.err.print(e);
        }
        try(PreparedStatement ps4 = CON.prepareStatement("UPDATE visitassp SET erogato=1 WHERE id_vssp=?")){
            ps4.setInt(1,id_vssp);
            ps4.executeUpdate();
            ps4.close();
        }catch(SQLException e){
            System.err.print(e);
        }
    }

    @Override
    public SSP getByEmailAndPassword(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) {
            throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));
        }
        
        try (PreparedStatement ps = CON.prepareStatement("SELECT * FROM loginssp WHERE email=?")) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            rs.next();
            int id_ssn = rs.getInt("id_ssp");
            String salt = rs.getString("salt");
            String hash = rs.getString("hash");
            Encrypt enc = new Encrypt();
            String res = "";
            try {
                res = enc.getHash(password, salt);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(JDBCMedicoBaseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.close();
            rs.close();
            if(res.equals(hash)){
                SSP ssp = getByPrimaryKey(id_ssn);
                return ssp;
            }
            return null;
        } catch (SQLException ex) {
            return null;
        } 
    }

    @Override
    public String getSaltByEmail(String mail) throws DAOException {
        String salt = null;
        try(PreparedStatement ps = CON.prepareStatement("SELECT salt FROM loginssp WHERE email=?")){
            ps.setString(1, mail);
            try(ResultSet rs = ps.executeQuery()){
                rs.next();
                salt = rs.getString("salt");
                rs.close();
                ps.close();
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the salt for the passed mail", ex);
        }
    }

    @Override
    public void updateHash(String hash, String mail) throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("UPDATE loginssp SET hash=? WHERE email=?")){
            ps.setString(1, hash);
            ps.setString(2, mail);
            ps.execute();
            ps.close();
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update", ex);
        }
    }
    
}