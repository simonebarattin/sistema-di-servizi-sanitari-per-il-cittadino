/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

/**
 *
 * @author simon
 */
public class Esame {
    
    private int id;
    
    private String tipo;
    
    private String erog;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getErog() {
        return erog;
    }

    public void setErog(String erog) {
        this.erog = erog;
    }
    
}
