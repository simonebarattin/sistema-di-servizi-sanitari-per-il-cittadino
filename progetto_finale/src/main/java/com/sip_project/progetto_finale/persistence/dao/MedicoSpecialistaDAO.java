/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.MedicoSpecialista;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author simon
 */
public interface MedicoSpecialistaDAO extends DAO<MedicoSpecialista, Integer>{
        
    public MedicoSpecialista getByEmailAndPassword(String email, String password) throws DAOException;
    
    public List<Paziente> getPatients() throws SQLException;

    public String getSaltByEmail(String mail) throws DAOException;

    public void updateHash(String hash, String mail) throws DAOException;
    
}
