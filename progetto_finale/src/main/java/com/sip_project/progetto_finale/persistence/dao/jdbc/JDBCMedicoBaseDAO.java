/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao.jdbc;

import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.commons.persistence.dao.jdbc.JDBCDAO;
import com.sip_project.progetto_finale.persistence.dao.MedicoBaseDAO;
import com.sip_project.progetto_finale.persistence.entities.MedicoBase;
import com.sip_project.progetto_finale.persistence.entities.Paziente;
import com.sip_project.progetto_finale.persistence.entities.Report;
import com.sip_project.progetto_finale.persistence.entities.VisitaMS;
import com.sip_project.progetto_finale.servlets.Encrypt;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simon
 */
public class JDBCMedicoBaseDAO extends JDBCDAO<MedicoBase, Integer> implements MedicoBaseDAO{
    
    public JDBCMedicoBaseDAO(Connection con){
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MedicoBase getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try(PreparedStatement ps = CON.prepareStatement("SELECT * FROM medicobase WHERE id_mb=?");){
            ps.setInt(1, primaryKey);
            try(ResultSet rs = ps.executeQuery();){
                rs.next();
                MedicoBase temp = new MedicoBase();
                temp.setId(rs.getInt("id_mb"));
                temp.setNome(rs.getString("nome"));
                temp.setCognome(rs.getString("cognome"));
                temp.setProvincia(rs.getString("sp"));
                temp.setCitta(rs.getString("citta"));
                rs.close();
                ps.close();
                return temp;
            }
        }catch(SQLException e){
            throw new DAOException("Impossible to get the user for the passed primary key", e);
        }
    }

    @Override
    public List<MedicoBase> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MedicoBase getByEmailAndPassword(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) {
            throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));
        }
        
        try (PreparedStatement ps = CON.prepareStatement("SELECT * FROM loginbase WHERE email=?")) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            rs.next();
            int id_mb = rs.getInt("id_mb");
            String salt = rs.getString("salt");
            String hash = rs.getString("hash");
            Encrypt enc = new Encrypt();
            String res = "";
            try {
                res = enc.getHash(password, salt);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(JDBCMedicoBaseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.close();
            rs.close();
            if(res.equals(hash)){
                PreparedStatement log = CON.prepareStatement("SELECT medicobase.id_mb,medicobase.nome,cognome,medicobase.sp,citta FROM "
                        + "loginbase JOIN medicobase ON loginbase.id_mb = medicobase.id_mb JOIN provincia ON medicobase.sp = provincia.sp WHERE medicobase.id_mb=?");
                log.setInt(1, id_mb);
                ResultSet ris = log.executeQuery();
                ris.next();
                MedicoBase mb = new MedicoBase();
                mb.setId(ris.getInt("id_mb"));
                mb.setNome(ris.getString("nome"));
                mb.setCognome(ris.getString("cognome"));
                mb.setProvincia(ris.getString("sp"));
                mb.setCitta(ris.getString("citta"));
                log.close();
                ris.close();
                return mb;
            }
            return null;
        } catch (SQLException ex) {
            return null;
        } 
    }
    @Override
    public List<Paziente> getPatients(int ib_mb) throws SQLException{
        List<Paziente> patients = new ArrayList<>();
        PreparedStatement ps = CON.prepareStatement("SELECT * FROM paziente WHERE id_mb=?");
        ps.setInt(1,ib_mb);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Paziente temp = new Paziente();
            temp.setSSN(rs.getString("ssn"));
            temp.setNome(rs.getString("nome"));
            temp.setCognome(rs.getString("cognome"));
            temp.setEmail(rs.getString("email"));
            temp.setSex(rs.getString("sesso").charAt(0));
            temp.setData(rs.getDate("datanascita"));
            temp.setFoto(rs.getString("foto_path"));
            temp.setProvincia(rs.getString("sp"));
            int id_mb = rs.getInt("id_mb");
            MedicoBase mb = new MedicoBase();
            PreparedStatement ps2 = CON.prepareStatement("SELECT * FROM medicobase WHERE id_mb=?");
            ps2.setInt(1,id_mb);
            ResultSet rs2 = ps2.executeQuery();
            rs2.next();
            mb.setId(rs2.getInt("id_mb"));
            mb.setNome(rs2.getString("nome"));
            mb.setCognome(rs2.getString("cognome"));
            mb.setProvincia(rs2.getString("sp"));
            mb.setCitta(rs2.getString("citta"));
            temp.setMb(mb);
            
            patients.add(temp);
        }
        rs.close();
        ps.close();
        return patients;
    }
    @Override
    public MedicoBase update(MedicoBase arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Report> getReportS(int id_mb,String ssn, List<VisitaMS> vms) throws SQLException {
    
        List<Report> report = new ArrayList<>();
        PreparedStatement ps = CON.prepareStatement("select * from reportvisitas join visitamedicospec on reportvisitas.id_vs=visitamedicospec.id_vs left join esame on visitamedicospec.id_es=esame.id_es join paziente on paziente.ssn=visitamedicospec.ssn where visitamedicospec.id_mb=? and paziente.ssn=?");
        ps.setInt(1,id_mb);
        ps.setString(2,ssn);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Report temp = new Report();
            temp.setErog(rs.getDate("data_erog"));
            //temp.setEsame(rs.); da aggiungere il nome dell'esame
            temp.setRisultato(rs.getString("risultato"));
            VisitaMS temps= new VisitaMS();
            temps.setAnamnesi(rs.getString("anamnesi"));
            temps.setEsame(rs.getString("tipo"));
            report.add(temp);
            vms.add(temps);
        }
        rs.close();
        ps.close();
        return report;
    }

    @Override
    public List<Report> getReportP(int id_mb,String ssn) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSaltByEmail(String mail) throws DAOException {
        String salt = null;
        try(PreparedStatement ps = CON.prepareStatement("SELECT salt FROM loginbase WHERE email=?")){
            ps.setString(1, mail);
            try(ResultSet rs = ps.executeQuery()){
                rs.next();
                salt = rs.getString("salt");
                rs.close();
                ps.close();
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the salt for the passed mail", ex);
        }
    }

    @Override
    public void updateHash(String hash, String mail) throws DAOException {
        try(PreparedStatement ps = CON.prepareStatement("UPDATE loginbase SET hash=? WHERE email=?")){
            ps.setString(1, hash);
            ps.setString(2, mail);
            ps.execute();
            ps.close();
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update", ex);
        }
    }
    
}
