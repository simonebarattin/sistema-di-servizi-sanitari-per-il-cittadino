/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.entities;

import java.sql.Date;

/**
 *
 * @author simon
 */
public class VisitaMS implements Comparable<VisitaMS>{
    
    private int id_vs,id_ms;
    private Date scadenza;
    private String anamnesi, esame, ms;
    private boolean completo;
    private Paziente paz;
    private MedicoBase mb;

    public String getMs() {
        return ms;
    }

    public void setMs(String ms) {
        this.ms = ms;
    }

    public int getId_vs() {
        return id_vs;
    }

    public void setId_vs(int id_vs) {
        this.id_vs = id_vs;
    }

    public int getId_ms() {
        return id_ms;
    }

    public void setId_ms(int id_ms) {
        this.id_ms = id_ms;
    }

    public Date getErog() {
        return scadenza;
    }

    public void setErog(Date erog) {
        this.scadenza = erog;
    }

    public String getAnamnesi() {
        return anamnesi;
    }

    public void setAnamnesi(String anamnesi) {
        this.anamnesi = anamnesi;
    }

    public boolean isCompleto() {
        return completo;
    }

    public void setCompleto(boolean completo) {
        this.completo = completo;
    }        

    public String getEsame() {
        return esame;
    }

    public void setEsame(String esame) {
        this.esame = esame;
    }

    public Paziente getPaz() {
        return paz;
    }

    public void setPaz(Paziente paz) {
        this.paz = paz;
    }

    public MedicoBase getMb() {
        return mb;
    }

    public void setMb(MedicoBase mb) {
        this.mb = mb;
    }

    @Override
    public int compareTo(VisitaMS o) {
        if (getErog()== null || o.getErog()== null)
            return 0;
        return getErog().compareTo(o.getErog());
    }
}
