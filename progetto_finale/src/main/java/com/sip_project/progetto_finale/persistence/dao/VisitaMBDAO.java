/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sip_project.progetto_finale.persistence.dao;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOException;
import com.sip_project.progetto_finale.persistence.entities.VisitaMB;
import java.util.List;

/**
 *
 * @author pietr
 */
public interface VisitaMBDAO extends DAO<VisitaMB, Integer>{
    public List<VisitaMB> getBySSN(String ssn) throws DAOException;
    
    public void updateAnamnesi(String anamnesi, int id_vs) throws DAOException;

    public List<VisitaMB> getAll(int id_mb);
}
