package com.sip_project.commons.persistence.dao.factories;

import com.sip_project.commons.persistence.dao.DAO;
import com.sip_project.commons.persistence.dao.exceptions.DAOFactoryException;

public interface DAOFactory {

    public void shutdown();
    
    public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoInterface) throws DAOFactoryException;
}
