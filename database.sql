drop database project;
create database project;
use project;
CREATE TABLE IF NOT EXISTS Provincia (
 SP char(2),
 nome varchar(30),
  PRIMARY KEY (SP)
);

CREATE TABLE if not exists SSP (
  ID_SSP int NOT NULL AUTO_INCREMENT,
  SP char(2),
  PRIMARY KEY (ID_SSP),
  foreign key (sp) references provincia(sp)
);

CREATE TABLE if not exists MedicoSpecialista (
  ID_MS int NOT NULL AUTO_INCREMENT,
  nome varchar(30),
  cognome varchar(30),
  SP char(2),
  PRIMARY KEY (ID_MS),
  foreign key (sp) references provincia(sp)
);

CREATE TABLE if not exists MedicoBase (
 ID_MB int NOT NULL AUTO_INCREMENT,
 nome varchar(30),
 cognome varchar(30),
 SP char(2),
 citta varchar(30),
  PRIMARY KEY (ID_MB),
  foreign key (sp) references provincia(sp)
);

CREATE TABLE IF NOT EXISTS Paziente (
 SSN char(16),
 nome varchar(30),
 cognome varchar(30),
 sesso char(1),
 email varchar(50),
 dataNascita date,
 foto_path varchar(100),
 SP char(2),
 ID_MB int,
  PRIMARY KEY (SSN),
  foreign key (SP) REFERENCES provincia(sp),
  foreign key (id_mb) references medicobase(id_mb)
);

CREATE TABLE if not exists LoginSpecialista (
  email varchar(50),
  ID_MS int,
  salt char(16),
  hash char(64),
  PRIMARY KEY (email),
  foreign key (id_ms) references medicospecialista(id_ms)
);

CREATE TABLE if not exists VisitaMedicoBase (
  ID_VB int NOT NULL AUTO_INCREMENT,
  anamnesi varchar(500),
  scadenza timestamp,
  completato boolean,
  erogato boolean,
  SSN char(16),
  ID_MB int,
  PRIMARY KEY (ID_VB),
  foreign key (ssn) references paziente(ssn),
  foreign key (id_mb) references medicobase(id_mb)
);


CREATE TABLE if not exists Esame (
  ID_ES int NOT NULL AUTO_INCREMENT,
  tipo varchar(100),
  erog char(1),
  PRIMARY KEY (ID_ES)
);

CREATE TABLE if not exists LoginUtente (
  SSN char(16),
  email varchar(50),
  salt char(16),
  hash char(64),
  PRIMARY KEY (email),
  foreign key (ssn) references paziente(ssn)
);

CREATE TABLE if not exists LoginSSP (
  ID_SSP int,
  email varchar(50),
  salt char(16),
  hash char(64),
  PRIMARY KEY (email),
  foreign key (id_ssp) references ssp(id_ssp)
);

CREATE TABLE if not exists VisitaSSP (
  ID_VSSP int NOT NULL AUTO_INCREMENT,
  scadenza timestamp,
  SSN char(16),
  completato boolean,
  erogato boolean,
  ID_SSP int,
  ID_ES int,
	primary key(id_vssp),
    foreign key (ssn) references paziente(ssn),
	foreign key (id_es) references esame(id_es) ,
    foreign key (id_ssp) references ssp(id_ssp)
);

CREATE TABLE if not exists VisitaMedicoSpec (
  ID_VS int NOT NULL AUTO_INCREMENT,
  anamnesi varchar(500),
  scadenza timestamp,
  completato boolean,
  erogato boolean,
  SSN char(16),
  ID_MS int,
  ID_MB int,
  ID_ES int,
  PRIMARY KEY (ID_VS),
  foreign key (ssn) references paziente(ssn),
  foreign key (id_es) references esame(id_es),
	foreign key (id_mb) references medicobase(id_mb),
    foreign key (id_ms) references medicospecialista(id_ms)
);


CREATE TABLE if not exists ReportVisitaS (
  ID_Report int NOT NULL AUTO_INCREMENT,
  ID_VS int,
  data_erog timestamp,
  risultato varchar(500),
  PRIMARY KEY (ID_Report),
  foreign key (id_vs) references visitamedicospec(id_vs)
);

CREATE TABLE IF NOT EXISTS TicketS (
	id_ts int NOT NULL AUTO_INCREMENT,
    ID_Report int,
    pagato boolean,
    data_erog timestamp,
    valore float,
    PRIMARY KEY (id_ts),
    foreign key (ID_Report) references ReportVisitaS(ID_Report)
);

CREATE TABLE if not exists ReportVisitaB (
  ID_Report int NOT NULL AUTO_INCREMENT,
  ID_VB int,
  data_erog timestamp,
  risultato varchar(500),
  PRIMARY KEY (ID_Report),
  foreign key (id_vb) references visitamedicobase(id_vb)
);

CREATE TABLE IF NOT EXISTS TicketB (
	id_tb int NOT NULL AUTO_INCREMENT,
    ID_Report int,
    pagato boolean,
    data_erog timestamp,
    valore float,
    PRIMARY KEY (id_tb),
    foreign key (ID_Report) references ReportVisitaB(ID_Report)
);

CREATE TABLE if not exists ReportEsameSSP (
  ID_Report int NOT NULL AUTO_INCREMENT,
  ID_VSSP int,
  data_erog timestamp,
  risultato varchar(500),
  PRIMARY KEY (ID_Report),
  foreign key (id_vssp) references visitassp(id_vssp)
);

CREATE TABLE IF NOT EXISTS TicketSSP (
	id_tssp int NOT NULL AUTO_INCREMENT,
    ID_Report int,
    pagato boolean,
    data_erog timestamp,
    valore float,
    PRIMARY KEY (id_tssp),
    foreign key (ID_Report) references ReportEsameSSP(ID_Report)
);

CREATE TABLE if not exists Farmaco (
  ID_FARM int NOT NULL AUTO_INCREMENT,
  nome varchar(30),
  PRIMARY KEY (ID_FARM)
);

CREATE TABLE if not exists PrescrizioneFarmaco (
  ID_RIC int NOT NULL AUTO_INCREMENT,
  SSN char(16),
  ID_FARM int,
  data_erog timestamp,
  ritirato boolean,
  PRIMARY KEY (ID_RIC),
  foreign key (ssn) references paziente(ssn),
  foreign key (id_farm) references farmaco(id_farm)
);

CREATE TABLE if not exists LoginBase (
  ID_MB int,
  email varchar(50),
  salt char(16),
  hash char(64),
  PRIMARY KEY (email),
  foreign key (id_mb) references medicobase(id_mb)
);

INSERT INTO provincia VALUES
    ('AG','Agrigento'),
    ('AL','Alessandria'),
    ('AN','Ancona'),
    ('AO','Aosta'),
    ('AQ','L''Aquila'),
    ('AR','Arezzo'),
    ('AP','Ascoli-Piceno'),
    ('AT','Asti'),
    ('AV','Avellino'),
    ('BA','Bari'),
    ('BT','Barletta-Andria-Trani'),
    ('BL','Belluno'),
    ('BN','Benevento'),
    ('BG','Bergamo'),
    ('BI','Biella'),
    ('BO','Bologna'),
    ('BZ','Bolzano'),
    ('BS','Brescia'),
    ('BR','Brindisi'),
    ('CA','Cagliari'),
    ('CL','Caltanissetta'),
    ('CB','Campobasso'),
    ('CI','Carbonia Iglesias'),
    ('CE','Caserta'),
    ('CT','Catania'),
    ('CZ','Catanzaro'),
    ('CH','Chieti'),
    ('CO','Como'),
    ('CS','Cosenza'),
    ('CR','Cremona'),
    ('KR','Crotone'),
    ('CN','Cuneo'),
    ('EN','Enna'),
    ('FM','Fermo'),
    ('FE','Ferrara'),
    ('FI','Firenze'),
    ('FG','Foggia'),
    ('FC','Forli-Cesena'),
    ('FR','Frosinone'),
    ('GE','Genova'),
    ('GO','Gorizia'),
    ('GR','Grosseto'),
    ('IM','Imperia'),
    ('IS','Isernia'),
    ('SP','La-Spezia'),
    ('LT','Latina'),
    ('LE','Lecce'),
    ('LC','Lecco'),
    ('LI','Livorno'),
    ('LO','Lodi'),
    ('LU','Lucca'),
    ('MC','Macerata'),
    ('MN','Mantova'),
    ('MS','Massa-Carrara'),
    ('MT','Matera'),
    ('VS','Medio Campidano'),
    ('ME','Messina'),
    ('MI','Milano'),
    ('MO','Modena'),
    ('MB','Monza-Brianza'),
    ('NA','Napoli'),
    ('NO','Novara'),
    ('NU','Nuoro'),
    ('OG','Ogliastra'),
    ('OT','Olbia Tempio'),
    ('OR','Oristano'),
    ('PD','Padova'),
    ('PA','Palermo'),
    ('PR','Parma'),
    ('PV','Pavia'),
    ('PG','Perugia'),
    ('PU','Pesaro-Urbino'),
    ('PE','Pescara'),
    ('PC','Piacenza'),
    ('PI','Pisa'),
    ('PT','Pistoia'),
    ('PN','Pordenone'),
    ('PZ','Potenza'),
    ('PO','Prato'),
    ('RG','Ragusa'),
    ('RA','Ravenna'),
    ('RC','Reggio-Calabria'),
    ('RE','Reggio-Emilia'),
    ('RI','Rieti'),
    ('RN','Rimini'),
    ('RM','Roma'),
    ('RO','Rovigo'),
    ('SA','Salerno'),
    ('SS','Sassari'),
    ('SV','Savona'),
    ('SI','Siena'),
    ('SR','Siracusa'),
    ('SO','Sondrio'),
    ('TA','Taranto'),
    ('TE','Teramo'),
    ('TR','Terni'),
    ('TO','Torino'),
    ('TP','Trapani'),
    ('TN','Trento'),
    ('TV','Treviso'),
    ('TS','Trieste'),
    ('UD','Udine'),
    ('VA','Varese'),
    ('VE','Venezia'),
    ('VB','Verbania'),
    ('VC','Vercelli'),
    ('VR','Verona'),
    ('VV','Vibo-Valentia'),
    ('VI','Vicenza'),
    ('VT','Viterbo');

insert into medicospecialista(nome,cognome,sp) values
	('Angelo','De Falco','NA'),
    ('Ercole','De Masi','RM'),
    ('Alberto','Poli','MI'),
    ('Francesco','Valenti','CT'),
    ('Luca','Faravelli','FI'),
    ('Alfredo Dioniso','Pansini','BA');
    
insert into loginspecialista values
	('angelodefalco@medicospecialista.com', 1, 'dsajgacwehzpavyr', '094ef52b6bab0d639de77ba0edbc6f255c5550b693713241db84bfd3d7293232'),
    ('ercoledemasi@medicospecialista.com', 2, 'rrmqhegxpsmcgafa','d610066659972b394add28f53d433e1f86808287fc6a7636857e24dd7733e042'),
    ('albertopoli@medicospecialista.com',3,'bsafxdntdbuvnefa','4ed83fee32d4ee76e4629731a4d52ec6c4990c897cb72fb33533525e68d6c39c'),
    ('francescovalenti@medicospecialista.com',4,'upcpvmbhjkqmdjeq','8af59e374969ab0c3cf65ad4d0244f6bf6a6ef85b0f79d582440d3e7ba4fcdc4'),
    ('lucafaravelli@medicospecialista.com',5,'nnxcbeqdjaezymen','20ab225b8e8e4b02b4fc3f289a0a888475e34f28fd5fb952a6b135f89ebf8241'),
    ('alfredodionisopansini@medicospecialista.com',6,'akkjtjhbtpngqwmn','c335a3d1e21ea853559ecbe9bc02489c180c3827d21512a956f5cc2c01b72d99');
    
insert into medicobase(nome,cognome,sp,citta) values
	('Roberto','Rodeghiero','BL','Belluno'),
    ('Antonio','Soverina','CZ','Catanzaro '),
    ('Arianna','Sandoni','BO','San Pietro in Casale'),
    ('Vincenzo','Pecoraro','RM','Fiano Romano'),
    ('Riccardo','Lucchesi','MI','Milano'),
    ('Pierluigi','Castellani','AQ','Lucoli'),
    ('Luigi','Renzullo','CB','Termoli'),
    ('Michele','Plantamura','BA','Santeramo in Colle'),
    ('Daniela','Lo Re','PA','Mezzojuso'),
    ('Donatella','Mattana','TN','Borgo Valsugana'),
    ('Chiara','Rosina','AO','Verrès'),
    ('Gerardo','Rosati','PZ','San Chirico Nuovo'),
    ('Gennaro','Alfano','CE','San Cipriano d\'Aversa'),
    ('Tiziano','Caprara','UD','San Giovanni al Natisone'),
    ('Giorgio','Granone','SV','Celle Ligure'),
    ('Pierluigi','Feliziani','AP','Ascoli Piceno'),
    ('Luana','Assanelli','AL','Villalvernia'),
    ('Pierfrancesco','Pisano','OR','Norbello'),
    ('Serena','Antenore','AR','Bucine'),
    ('Giulio','Franceschini','PG','Bastia Umbra'),
    ('Porzia','Misserville','BL','Belluno'),
	('Ernestina','Torlonia','BL','Feltre'),
	('Diamante','Gebissi','TN','Trento'),
	('Marco','Laines','TN','Rovereto'),
	('Sesto','Fusillo','BL','Agordo');
    
insert into loginbase(id_mb,email,salt,hash) values
	(1,'pietro.ghislieri@studenti.unitn.it','zagpyucaqzqxwmch','529ecde5f1c038b0e0c8e833f6e100ba375e27c29f45f446d232c42a0c125b75'),
    (2,'antoniosoverina@medicobase.com','sncuzyavsdjxemur','1ca8ab0b98ae16638b88a35618ccf2f8d75eb731ff4acc60511ff6e7e4c82f04'),
    (3,'ariannasandoni@medicobase.com','vbuptndsghqmtdhx','ac31b8a29d28853e311725c2f509186885d8abc78a594bafc92d44b417d0ae32'),
    (4,'vincenzopecoraro@medicobase.com','tujvagqwrrvafgnj','04f728892579c3dd2a9ee1d12655197ac4f9b26ac6f94180b452c22f7d8ee1fc'),
    (5,'riccardolucchesi@medicobase.com','uaadabjknyyygmkv','da7c3523c04f9791f301637e2368dda203f6feaae30a69a0bd42b331129c6503'),
    (6,'pierluigicastellani@medicobase.com','gqsdzdxyqhmmktmn','220c4d1dbfacf5cbf24719c445ed8f214ec94ca234ac624e392b68fd7c450410'),
    (7,'luigirenzullo@medicobase.com','wnyrwsuhcpgyzmww','230602b943cd7204ad374e5e041270b24ba90a8bda002dddd1f2ff5305aee943'),
    (8,'micheleplantamura@medicobase.com','dmndzwtzymwtasyr','62b868d4fdea72ae2fc5e5d8e4f67fdb931f75f1f739dd7ca4d6c84c63a7908d'),
    (9,'danielalore@medicobase.com','nwdaynarnmqcbfkz','f47bb536be6dbbbd4a7ed3834239542c3c8d1e844f4666409dad9982b1c00fb6'),
    (10,'donatellamattana@medicobase.com','pebbafvqvxynkrzy','60ae9a9c3f04e5ce55e4efa14216d8d9a97bcaf87cf79c0acab6a0bf1b3d01a8'),
    (11,'chiararosina@medicobase.com','pupwpcxxcmmtkfab','44386fe99601fadf5f24ffe7fbfc313c42c5cd97c1707106e2c02c4a42f6390a'),
    (12,'gerardorosati@medicobase.com','dhdzkwwxrpmsweee','20ee0ccb03563da07dd5cb13a3495d48e6ccec8f9a9068014fb2da35c1477161'),
    (13,'gennaroalfano@medicobase.com','hcxgcjqzbvwexgaw','a71e703215246601d40a1d81bbe4d9069437c8f4434c297ef463d64b33565fb5'),
    (14,'tizianocaprara@medicobase.com','pqnzxxdwfcnyxqdy','51fcd1873eb067a25f543b8c006683aba91d9ce1dac49f05adcb06cda9b2405b'),
    (15,'giorgiogranone@medicobase.com','kfajwtdyhmwgbmmq','9007f237e66147da5fca96fb3d0b32f00812646def4af153acee5fd8def42952'),
    (16,'pierluigifeliziani','vgumvmuhgzvbmfrj','243aaed78585923ebd7fafbb9ae6c1c5d0dc46d07458136a64207eb9790d6e76'),
    (17,'luanaassanelli','abfewysufnckyjvw','f06e11107b6a48f79f63d9793f573479db039d95742d030db65ac2533c5a29b6'),
    (18,'pierfrancescopisano@medicobase.com','cjcytkswnjhehukh','24e71acf63246f9b385795434927aecc05409601b59174aab954ac14bd8dd466'),
    (19,'serenaantenore@medicobase.com','cmaxrgukawxkgkgz','8aca234a32ff9c77dd9a3bbb8692f2495d08821d54cb8938cf26db23a3a3f02a'),
    (20,'giuliofranceschini@medicobase.com','kjcvvjukbmvzufts','eef4fab4daf8536b5b843652164588bd83081ea32c57f4986cdf4aeaeb9a0342'),
    (21,'mssprz85t65e852e@yahoo.it','huywqdyqjzxzbhey','8bfd90acc1cd5028a33fd2f77da6347836b53bb5f8bb7cb2c9a3384a45535673'),
    (22,'trlrst74a63d832s@gmail.com','vsermucyfwfaytdm','a1d370a55d51c3b5792fe990967d69ab993f166f1673489dec9c5282585a7b85'),
    (23,'gbsdnt76c07a273e@hotmail.it','bdgnqkeuczdnbjzv','cc4332d984925c2fce0d9112e9c88936cefdac1ae6f43b28f52d9bbd20820d41'),
    (24,'lnsmrc64c02c976v@gmail.com','svctbaxkjajhqwms','c23ea3dc7153b578e02aa5d6fdabe1d708cce096d9e211ebb7eae51a117e5f41'),
    (25,'fslsst73s11a561g@gmail.com','gvbgupjbmbrxradd','aca0500156fdcee63db9c950c34226006bd390c8426735274e8b3fb2fdc24b6e');
    
insert into paziente(ssn,nome,cognome,sesso,email,datanascita,foto_path,sp,id_mb) values 
	('BRTSMN98R28D520C','Simone','Barattin','M','simonebarattin98@gmail.com',STR_TO_DATE('28-10-1998', '%d-%m-%Y'),'male1.jpg','BL',1),
	('TRBCSL33M66G958A','Consolata','Tarabella','F','trbcsl33m66g958a@gmail.com','1933-08-26','46.jpg','BL',1),
	('MDRTTL95S29A472Z','Attilio','Maderloni','M','mdrttl95s29a472z@yahoo.it','1995-11-29','25.jpg','TN',3),
	('GCCRNN37R70A677L','Ermanna','Giaccardi','F','gccrnn37r70a677l@hotmail.it','1937-10-30','19.jpg','BL',2),
	('RCSRNG50R12B798I','Arcangelo','Racis','M','rcsrng50r12b798i@yahoo.it','1950-10-12','34.jpg','TN',4),
	('PLLLSS63R68L485V','Alessia','Pelligra','F','plllss63r68l485v@gmail.com','1963-10-28','30.jpg','BL',1),
	('MLDBFC00T25H846X','Bonifacio','Melideo','M','mldbfc00t25h846x@gmail.com','2000-12-25','26.jpg','TN',3),
	('CPRSDN54S19C144C','Sidonio','Caprarella','M','cprsdn54s19c144c@gmail.com','1954-11-19','13.jpg','BL',5),
	('SDLFDL72P09M181B','Fedele','Saidel','M','sdlfdl72p09m181b@gmail.com','1972-09-09','38.jpg','BL',2),
	('CPLNCI32H15F614Q','Nico','Capola','M','cplnci32h15f614q@gmail.com','1932-06-15','12.jpg','BL',5),
	('SRSBNC43A48C838N','Berenice','Asarise','F','srsbnc43a48c838n@email.it','1943-01-08','43.jpg','TN',4),
	
	('SFRFLC54A63C131U','Felicia','Sfreddo','F','sfrflc54a63c131u@gmail.com','1954-01-23','39.jpg','TN',3),	
	('QTZCRN63R62C943M','Caterina','Quatuzzo','F','qtzcrn63r62c943m@yahoo.it','1963-10-22','33.jpg','BL',5),	
	('ZBRRST41S57C734Z','Ernestina','Zebiri','F','zbrrst41s57c734z@email.it','1941-11-17','48.jpg','BL',1),
	('RFLRME75B23L243K','Remo','Arfelli','M','rflrme75b23l243k@yahoo.it','1975-02-23','36.jpg','BL',5),
	('PRLGST72D55M148K','Augusta','Parollo','F','prlgst72d55m148k@gmail.com','1972-04-15','31.jpg','TN',4),
	('LJNTZN70B11H187I','Tiziano','Lajone','M','ljntzn70b11h187i@gmail.com','1970-02-11','24.jpg','BL',2),
	('SLTFNC44A16B217O','Francesco','Saltarelli','M','sltfnc44a16b217o@gmail.com','1944-01-19','40.jpg','TN',3),
	('RFFMCR49C25I316Z','Macario','Raffaghello','M','rffmcr49c25i316z@gmail.com','1949-03-25','35.jpg','BL',1),
	('GRGMNA55R52I809H','Amina','Gargussi','F','grgmna55r52i809h@gmail.com','1955-10-12','21.jpg','TN',4),
	('RSSDNC74R03D893H','Domenico','Russospena','M','rssdnc74r03d893h@hotmail.it','1974-10-03','37.jpg','BL',2),
	
	('BLGGRS45E65L872M','Generosa','Blagho','F','blggrs45e65l872m@gmail.com','1945-05-25','2','BL',1),
	('FRRVDR34D23H017G','Evandro','Ferrucciu','M','frrvdr34d23h017g@yahoo.it','1934-04-23','18.jpg','TN',3),
	('CHSNRT88H11H076J','Onorato','Chiascione','M','chsnrt88h11h076j@hotmail.it','1988-06-11','8.jpg','BL',2),
	('GRZGVS62C03E493R','Gervaso','Gurizzan','M','grzgvs62c03e493r@yahoo.it','1962-03-03','22.jpg','TN',4),
	('FRRCNT64M48D508S','Chantal','Ferra','F','frrcnt64m48d508s@gmail.com','1964-08-08','17.jpg','BL',1),
	('STNMLN98P61A995F','Milena','Steinhauser','F','stnmln98p61a995f@gmail.com','1998-09-21','45.jpg','BL',5),
	('DNAGAI93L17H192Z','Gaio','Dani','M','dnagai93l17h192z@gmail.com','1993-07-17','14.jpg','TN',3),
	('CLSRSL48A61B555U','Rosalia','Calsineri','F','clsrsl48a61b555u@gmail.com','1948-01-21','9.jpg','BL',5),
	('CNFDRT80S14G101E','Doroteo','Cianfruglia','M','cnfdrt80s14g101e@gmail.com','1980-11-14','11.jpg','BL',2),
	('PRSDTT48M70H028K','Diletta','Persendi','F','prsdtt48m70h028k@email.it','1948-08-30','32','TN',4),
	
	('SRRLLE47L51B511S','Lelia','Serroni','F','srrlle47l51b511s@gmail.com','1947-07-11','42.jpg','BL',5),
	('CFLMRL81T18A770U','Maurilio','Cefola','M','cflmrl81t18a770u@yahoo.it','1981-12-18','6.jpg','TN',3),
	('DVNSVS00H29C390Y','Silvestro','Divincenzo','M','dvnsvs00h29c390y@email.it','2000-06-29','15.jpg','BL',1),
	('CHRTRS95H45B949Q','Teresa','Chersicla','F','chrtrs95h45b949q@yahoo.it','1995-06-05','7.jpg','TN',4),
	('VYELVR69S06F153T','Alvaro','Vey','M','vyelvr69s06f153t@gmail.com','1969-11-06','47.jpg','BL',2),
	('BJRDVD83C13E621L','Davide','Bajrami','M','bjrdvd83c13e621l@gmail.com','1983-03-13','1.jpg','TN',3),
	('BRSNBL45R44H991V','Annabella','Brasei','F','brsnbl45r44h991v@gmail.com','1945-10-04','5.jpg','BL',5),
	('NBBDTT61S59E461A','Odetta','Nebbiai','F','nbbdtt61s59e461a@gmail.com','1961-11-19','28.jpg','BL',1),
	('STLRSL98B47H599T','Rossella','Stiles','F','stlrsl98b47h599t@gmail.com','1998-02-07','44.jpg','TN',4),
	('FNRGNN46B05I043R','Giovanni','Fenaroli','M','fnrgnn46b05i043r@hotmail.it','1946-02-05','16.jpg','BL',2),
	
	('PLCNRD75H46F614B','Igrid','Pulcani','F','plcnrd75h46f614b@gmail.com','1975-06-06','29.jpg','TN',3),
	('GSPJTH69E23D417I','Jonathan','Gasperinatti','M','gspjth69e23d417i@yahoo.it','1969-05-23','23.jpg','BL',1),
	('BLSCST57C01C361S','Celestino','Blasi','M','blscst57c01c361s@hotmail.it','1957-03-01','3.jpg','BL',5),
	('BRCFMN62A26C791R','Filomeno','Birce','M','brcfmn62a26c791r@yahoo.it','1962-01-26','4.jpg','TN',4),
	('CLTTZN55E31I778K','Tiziano','Celotti','M','clttzn55e31i778k@gmail.com','1955-05-31','10.jpg','BL',2),
	('SPTGUO33A25G917S','Ugo','Spaetti','M','sptguo33a25g917s@gmail.com','1933-01-25','41.jpg','BL',5),
	('GPLPRD77L14F813A','Paride','Gipli','M','gplprd77l14f813a@gmail.com','1977-07-14','20.jpg','TN',3),
	('MNGGNN94P51A071W','Giovanna','Amingoni','F','mnggnn94p51a071w@email.it','1994-09-11','27.jpg','TN',4);

    
insert into loginutente(ssn,email,salt,hash) values 
	('BRTSMN98R28D520C','simonebarattin98@gmail.com','cyycrjkvjnbwemzu','5ec4e61e3bfa9ea6972bdbdd13fae44a53ab6d76ff9ef416ea7f1b8bf2f21656'),
    ('TRBCSL33M66G958A','trbcsl33m66g958a@gmail.com','cmgtuzrxtvxznfgf','286623d989b58e1db6a646f6c46ac8513bd3974d36975fa13e2a93fc0ecec82c'),
	('MDRTTL95S29A472Z','mdrttl95s29a472z@yahoo.it','xkuctwunvcdhpgmw','f36ace8df00b87d62dfa9ca2e9c421b32d559ec01b64081577104b54f30ca1eb'),
	('GCCRNN37R70A677L','gccrnn37r70a677l@hotmail.it','xpgjcqwmumzebnjm','d268f01ba1015363a19d710227850d0074d76c56813854258519fbe95a5fffe2'),
	('RCSRNG50R12B798I','rcsrng50r12b798i@yahoo.it','mhfxnvmpueapqwzk','269c1da4ba7a3b629ce294dbb44b744c841d211c6185f55a0a4a666ef8572c7e'),
	('PLLLSS63R68L485V','plllss63r68l485v@gmail.com','karrmkzajpexwcuj','bf102836136921ca2bb2c5adc82015e971a294d6636ccddc6c2545f3c94ef92d'),
	('MLDBFC00T25H846X','mldbfc00t25h846x@gmail.com','ycjrjzmegramzbms','7dd356848770d639f6a1b7a9a73a2f80ef55128b21fd21148340b8f459b85bef'),
	('CPRSDN54S19C144C','cprsdn54s19c144c@gmail.com','shdknjbavyezqwja','0e0c2f78f321ad822e15eb4dbaddf46b397e79a818d7c6549d301c363d62b69b'),
	('SDLFDL72P09M181B','sdlfdl72p09m181b@gmail.com','swheaapbpamzcccp','43b2b9d858bc4847e60cd6e72456f9db6adedba8d6ae06e38834192baafa7732'),
	('CPLNCI32H15F614Q','cplnci32h15f614q@gmail.com','ncdvbgcgtcmujrjj','445b2e4eb951ad193cfab95cb0f08a485013706d5cc68b2b8bd34761accd0cbc'),
	('SRSBNC43A48C838N','srsbnc43a48c838n@email.it','uxsfjqbedkpryxbk','d8512e33d3ce6faba43815cd912ce41ece4982b37e99e09cf52049f6c2c7b21b'),
	
	('SFRFLC54A63C131U','sfrflc54a63c131u@gmail.com','faubeqfrdrqmungh','f684575cf3e2bee553d01ebb8dd858685a61f1e3f7d2e3d0d1ae25c5d463a41b'),
	('QTZCRN63R62C943M','qtzcrn63r62c943m@yahoo.it','jhudhhtrnccmdryd','7099933d2e68a1220a19dac80eaf3665ce6bee945e3e07d20ab3c5e99f094ecb'),
	('ZBRRST41S57C734Z','zbrrst41s57c734z@email.it','hrxetssgrzkhwxnq','972927f9fce96b5641a1808e029a9850ded04954efdfbaf4dd4ecf9a451da48c'),
	('RFLRME75B23L243K','rflrme75b23l243k@yahoo.it','agnmwncspqrfwxvq','3bf965767fb92ec5187f6b5fb39da94ef04e33c2ebd54afd248d5879c0c456c5'),
	('PRLGST72D55M148K','prlgst72d55m148k@gmail.com','sbvvnjbvydbesays','391b236edc54ebe9eee06625995f59ea054f2a014a23df171e65db17493c5460'),
	('LJNTZN70B11H187I','ljntzn70b11h187i@gmail.com','wzmrxyvpfvbpkqzk','174b19c738848f9f0dde2ea906a6f191437916391890b42923b0d6152ddc301c'),
	('SLTFNC44A16B217O','sltfnc44a16b217o@gmail.com','hgbnnzreanqsvxaj','715a8fa13e5b3c0bb0994df728e60e68e7a2fb0cb3e802dd8f0528999344e0bb'),
	('RFFMCR49C25I316Z','rffmcr49c25i316z@gmail.com','dequccppeqtnjuqd','8e75e68a0445de90ae823f00dbae5fefc7921c7b3eefe8e3aa21af1f34de0393'),
	('GRGMNA55R52I809H','grgmna55r52i809h@gmail.com','shnejwkfrmbpzjrb','060de28669579d8ec2214d4d45716bb40e23e7b309de442dc87b850e63774e4b'),
	('RSSDNC74R03D893H','rssdnc74r03d893h@hotmail.it','yectsjkbgyqjxtbr','bf49ab5e914d9ecd174fbf54db0b9466d26f8190479253d6a6c518b61dc200c0'),
	
	('BLGGRS45E65L872M','blggrs45e65l872m@gmail.com','tgsuqjcjxchqdhrj','9067cd805f8f24f7a2c6a65ac9f3e97e283664a6bc712287ccf29faff351a1d4'),
	('FRRVDR34D23H017G','frrvdr34d23h017g@yahoo.it','awjgbhzrdtmrqvzs','f2685bc249c52b89fd251769a739cb603d014072df99a4222078e6f0ff6e3bf5'),
	('CHSNRT88H11H076J','chsnrt88h11h076j@hotmail.it','gvfygwhvcfcqjccw','9044d20f505076280a27e2cafb2280ff9f96194bdca0bab05d2ab79aeb035e5b'),
	('GRZGVS62C03E493R','grzgvs62c03e493r@yahoo.it','yzgtrqhfuhpqpjfs','e0a10ab54c2b34122e3b2b8f17696c5bc64b1bd62f6ca1bfc801eed390779dc8'),
	('FRRCNT64M48D508S','frrcnt64m48d508s@gmail.com','mgthpxeufshuystp','0cbcb6ab0b8b85e2f6d82494fa7c18794042026311969a4ed4e1c8002018455f'),
	('STNMLN98P61A995F','stnmln98p61a995f@gmail.com','zqvrwczzrpxxqmjf','88f1260166e25ed7b7fc548f276962e93649ae716151651613df6f3a675a1d49'),
	('DNAGAI93L17H192Z','dnagai93l17h192z@gmail.com','fqjzwavjtywaqftz','1c67bcad278743ffd7a400700f90d039327c160b77985a22ffa08efcd4df186e'),
	('CLSRSL48A61B555U','clsrsl48a61b555u@gmail.com','sxzfdjrckyqhnvpd','8dbf2e697dd49733461cea7e08d1643d0a72b6fdfd8c7f30507680d998357dd0'),
	('CNFDRT80S14G101E','cnfdrt80s14g101e@gmail.com','rguapnmkygdcrfsq','3ab803a22f4a8f6ae6a4fdd136ff73dd091cfbd0b1e17dd30a8d62b61f951f69'),
	('PRSDTT48M70H028K','prsdtt48m70h028k@email.it','rhdwvzuuurrrehbd','c68002c79f32cb6db88e586c44a3dc3cb88acf6cef07cf23ab92121cadd62671'),
	
	('SRRLLE47L51B511S','srrlle47l51b511s@gmail.com','yfqsnhaxaptmnvhv','56a3546de9488ee7510764f639bf509102bd00006fa02a9c01b4266760a6a1a1'),
	('CFLMRL81T18A770U','cflmrl81t18a770u@yahoo.it','prambtjbxjyxyexg','a3d1cdc9cb3c03d4adb5cdab97907b36c0042b7674b16b4a3895b6f9490d7b95'),
	('DVNSVS00H29C390Y','dvnsvs00h29c390y@email.it','ubcfnemafjydhchk','ff72b23920b615397d3849b01c1011c674530a8bedd8e20247790ae5e13d68e8'),
	('CHRTRS95H45B949Q','chrtrs95h45b949q@yahoo.it','kprnsqdpujjcaugs','9b7989d1bed9fcb8328f355ab0b6d7a6222d64bf3f900efb6f1df0b156043a21'),
	('VYELVR69S06F153T','vyelvr69s06f153t@gmail.com','mpyqqvvmzjfqsjzm','fd710d2d982b595aaf9bfb88ffaee3d89dfaa2eb12bc2acee3a44f496ed6e2fd'),
	('BJRDVD83C13E621L','bjrdvd83c13e621l@gmail.com','afhdnysesfmnxbvp','07ba918f58865100a83280c0a7c152cd1be6f36eb351fde8358c127f83ef6aaa'),
	('BRSNBL45R44H991V','brsnbl45r44h991v@gmail.com','uabpxvyerpsphdyj','9fbf3e3fda97c8188047fec1b7eb77ad07cfc77cd5c356719adb6ac648a7c476'),
	('NBBDTT61S59E461A','nbbdtt61s59e461a@gmail.com','kerthrwwrhgwjspm','7a171c429588d42c2c7ad94985c9088a065134aef7eaa6cf52fb47d79b18a3ed'),
	('STLRSL98B47H599T','stlrsl98b47h599t@gmail.com','uddrsrayerjrgxea','5e38294e905aef55c8244f525782b77709e7264a7e50e2ad73d3c0461fb708e7'),
	('FNRGNN46B05I043R','fnrgnn46b05i043r@hotmail.it','ccucpnpcsgfygccu','ab2386acc5e8a7be8391908da42dbdf8f9b580eed7ab92d7b4335aa162cb2414'),
	
	('PLCNRD75H46F614B','plcnrd75h46f614b@gmail.com','wrptvmhyajgwjanq','ea7708ffc01ce4f78687c5a1d5adde6bbdfbbff2b627623a1b60079262315a18'),
	('GSPJTH69E23D417I','gspjth69e23d417i@yahoo.it','crcxjsuttubtdeum','d96c8b9bb518781427eb6d8cad67c2ed1796f9bc3b63b742edf6b163d684c899'),
	('BLSCST57C01C361S','blscst57c01c361s@hotmail.it','tcpdttqkuvwpvved','878c7742dc6b94344dfb480f19f1d5c7140404861cd66306b80f31e375faa3e5'),
	('BRCFMN62A26C791R','brcfmn62a26c791r@yahoo.it','bayekuekcxdrhjvd','87a938b8ecb5afff859500ac729bf8af0eb2d04bc12670198a5693d1ae099ba0'),
	('CLTTZN55E31I778K','clttzn55e31i778k@gmail.com','myxgtebwuabsayqe','5a5345126cbf924a4b5ae6fa57345aa8b5c1f26d18d883ad7a0119c77e167b61'),
	('SPTGUO33A25G917S','sptguo33a25g917s@gmail.com','qgdgahvdzhrxnpzf','025fd11da9493ab39e231074d43636ba9cc8d69e5b195bb588cc7fc25d366e01'),
	('GPLPRD77L14F813A','gplprd77l14f813a@gmail.com','nzcfnmsnggvjwxcs','8744846b930c1a1f73489ad4ef4ef321a4194a5506d3f70dee3c4a0cb0104abb'),
	('MNGGNN94P51A071W','mnggnn94p51a071w@email.it','hbtakmgygbkbspnv','756f58879ab967939b060b32332a532e69126dbd1cacbe9ad0410c4076507b4b');

insert into esame(tipo,erog) values
	('Gengivoplastica asportazione di tessuto della gengiva','s'),
    ('Levigatura delle radici','s'),
    ('Intervento chirurgico preprotesico','s'),
    ('Asportazione di lesione dentaria della mandibola','s'),
	('Trattamento ortodontico con apparecchi mobili','s'),
    ('Analisi del sangue','p');
    
insert into visitamedicospec(scadenza,completato,erogato,ssn,id_ms,id_mb,id_es) values
	(STR_TO_DATE('28-10-2019', '%d-%m-%Y'), false, false, 'BRTSMN98R28D520C', 1, 1, null),
    (now(), false, false, 'BRTSMN98R28D520C', 1, 1, 2),
    (STR_TO_DATE('12-11-2020', '%d-%m-%Y'), false, false, 'BRTSMN98R28D520C', 1, 1, null);
    
insert into farmaco(nome) values
	('tachipirina 1000'),
    ('aspirina'),
	('okitask'),
	('tachipirina'),
	('simvastatina'),
	('omeprazolo'),
	('levotiroxina'),
	('ramipril'),
	('amlodipina'),
	('paracetamolo'),
	('atorvastatina'),	
	('sulbutamolo'),
	('lasoprazolo'),
	('metformina'),
	('colecalciferolo'),
	('amoxicillina'),	
	('brufen'),
	('plaquenil'),
	('pradaxa'),
	('pantopranzolo'),
	('klacid'),
	('dentalcortene'),	
	('bentelan'),
	('clenil'),
	('rinazina'),
	('enterogermina'),
	('voltaren'),	
	('maalox'),
	('moment'),
	('biochetasi'),
	('pursennid'),
	('buscopan'),	
	('vicks'),
	('bronchenolo'),
	('iridina'),
	('vivin c'),	
	('seki'),	
	('nurofen'),
	('benagol'),
	('imodium');
    
insert into prescrizionefarmaco(ssn,id_farm,data_erog,ritirato) values
	('BRTSMN98R28D520C',2,now(),0),
	('BRTSMN98R28D520C',1,now(),0);
    
insert into ssp(sp) values
	('BL'),
    ('TN');
    
insert into visitassp(scadenza,ssn,completato,erogato,id_ssp,id_es) values
	(now(),'BRTSMN98R28D520C',1,0,1,1);
    
insert into loginssp(id_ssp,email,salt,hash) VALUES
	(1,'serviziobl@ssp.it','psejrumrturwzyzx','f8759f1b7dd7541ac5df3738971c064cbacbe386f85a1fc7dc240553e6d2219d'),
	(2,'serviziotn@ssp.it','fppwtqjccvfjamsm','0d4a0e9bcbea2ee9def07a359ebc12674aae973864b21e6e6bef354dee65a93e');